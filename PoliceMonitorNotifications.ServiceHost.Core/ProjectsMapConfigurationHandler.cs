using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
        public class ProjectsMapConfigurationHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
                        return new ProjectsMapConfiguration(section);
		}
	}

        public class ProjectsMap
        {
                public readonly string V1Project;
                public readonly string V1ProjectScopeID;
                public string V1DefectTemplate;
                private string _v1ProjectPath;
                public readonly string MantisProject;
                public readonly bool MantisRecursive;
                public readonly bool V1Recursive;
                public readonly XmlElement Config;
                public string _IsVersionOneSideInitialRunComplete;
                public string _IsMantisSideInitialRunComplete;
                public bool _hasValidTemplate = true;
                public bool _IsV1ProjectExists = true;
                public bool _IsV1ProjectClosed = true;

                public bool _IsMantisProjectExists = true;
                public bool _IsMantisProjectEnabled = true;

                public bool _IsV1ProjectNameExists = true;
                public string _lastV1CheckDate;
                 
                public string V1ProjectPath
                {
                    get { return _v1ProjectPath; }
                    set { _v1ProjectPath = value; }
                }

                public string ISVersionOneSideInitialRunComplete
                {
                    get { return _IsVersionOneSideInitialRunComplete; }
                    set { _IsVersionOneSideInitialRunComplete = value; }
                }
                public string ISMantisSideInitialRunComplete
                {
                    get { return _IsMantisSideInitialRunComplete; }
                    set { _IsMantisSideInitialRunComplete = value; }
                }

                public bool HasValidTemplate
                {
                    get { return _hasValidTemplate; }
                    set { _hasValidTemplate = value; }
                }
                public string LastV1CheckDate
                {
                    get { return _lastV1CheckDate; }
                    set { _lastV1CheckDate = value; }
                }

                public bool ISV1ProjectExists
                {
                    get { return _IsV1ProjectExists; }
                    set { _IsV1ProjectExists = value; }
                }
                
                public bool ISV1ProjectClosed
                {
                    get { return _IsV1ProjectClosed; }
                    set { _IsV1ProjectClosed = value; }
                }

                public bool ISMantisProjectExists
                {
                    get { return _IsMantisProjectExists; }
                    set { _IsMantisProjectExists = value; }
                }
                public bool ISMantisProjectEnabled
                {
                    get { return _IsMantisProjectEnabled; }
                    set { _IsMantisProjectEnabled = value; }
                }

                public bool ISV1ProjectNameExists
                {
                    get { return _IsV1ProjectNameExists; }
                    set { _IsV1ProjectNameExists = value; }
                }
            
                public ProjectsMap()
                {
                }
                public ProjectsMap(string name, XmlElement config)
                {
                        //V1 configuration
                        V1Project = config["V1Project"].InnerText;
                        try
                        {
                                if (config["V1Project"].GetAttribute("includeChildren").ToLower() == "yes")
                                {
                                        V1Recursive = true;
                                }
                                else
                                {
                                        V1Recursive = false;
                                }
                        }
                        catch
                        {
                                V1Recursive = false;
                        }

                        try
                        {
                            if (!String.IsNullOrEmpty(config["V1Project"].GetAttribute("v1ProjectScopeID")))
                            {
                                V1ProjectScopeID = config["V1Project"].GetAttribute("v1ProjectScopeID").Trim();
                            }
                            else
                            {
                                V1ProjectScopeID = String.Empty;
                            }
                        }
                        catch
                        {
                            V1ProjectScopeID = String.Empty;
                        }

                        try
                        {
                            if (config["V1Project"].HasAttribute("defectTemplate"))
                            {
                                if (!String.IsNullOrEmpty(config["V1Project"].GetAttribute("defectTemplate")))
                                {
                                    V1DefectTemplate = config["V1Project"].GetAttribute("defectTemplate").Trim();
                                }
                                else
                                {
                                    V1DefectTemplate = String.Empty;
                                }
                            }
                            else
                            {
                                V1DefectTemplate = "NotDefined";
                            }
                        }
                        catch
                        {
                            V1DefectTemplate = String.Empty;
                        }


                        //Mantis configuration
                        MantisProject = config["MantisProject"].InnerText;

                        try
                        {
                                if (config["MantisProject"].GetAttribute("includeChildren").ToLower() == "yes")
                                {
                                        MantisRecursive = true;
                                }
                                else
                                {
                                        MantisRecursive = false;
                                }
                        }
                        catch
                        {
                                MantisRecursive = false;
                        }

                        
                        Config = config;                    
                        //Console.WriteLine("Loaded mapping : V1: " + V1Project + " <-> Mantis: " + MantisProject);
                }
        }

        internal class ProjectsMapConfiguration : List<ProjectsMap>
	{

		public ProjectsMapConfiguration(XmlNode section)
		{
                        XmlAttribute attrib = section.Attributes["class"];
                        if (attrib == null)
                        {
                            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ProjectsMapConfiguration_Debug_1") + section.Name + ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ProjectsMapConfiguration_Debug_2"));
                                return;
                        }

                        attrib = null;
                        attrib = section.Attributes["refProjectMap"];
                        if (attrib != null)
                        {
                            XmlDocument _xmlDocument = new XmlDocument();
                            _xmlDocument.Load(attrib.Value);

                            XmlNode node = _xmlDocument.SelectSingleNode("/configuration/ProjectsMap");
                            if (node != null)
                                section = node;
                        }

			foreach (XmlNode child in section.ChildNodes)
			{
				if (child.NodeType != XmlNodeType.Element)
					continue;



				try
				{
			//		IHostedService svc = (IHostedService)Activator.CreateInstance(type);
                                        Add(new ProjectsMap(child.LocalName, (XmlElement)child));
					//Console.WriteLine("Loaded {0}.", attrib.Value);
				}
				catch (Exception ex)
				{
                    Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ProjectsMapConfiguration_Exception_1"), attrib.Value, ex);					
				}
			}
		}
	}
}