using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
     public class ConnectorConfigurationHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
            return new ConnectorConfiguration(section);
		}
	}

      
    public class ConnectorConfiguration 
	{

        public ConnectorConfiguration(XmlNode section)
        {
            foreach (XmlNode child in section.ChildNodes)
            {
                if (child.NodeType != XmlNodeType.Element)
                    continue;


                if (child.Name.ToLower() == "GetAndPostLastV1CheckDateInMantis".ToLower())
                {
                    if (!String.IsNullOrEmpty(child.InnerText))
                    {
                        if (child.InnerText.ToLower() == "yes" || child.InnerText.ToLower() == "true" || child.InnerText == "1")
                        {
                            UseProfileFileMode.Instance.GetAndPostLastV1CheckDateInMantis = true;
                        }
                    }
                    else
                    {
                        throw new ConfigurationErrorsException(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConnectorConfiguration_Exception_3"), section);
                    }
                }

            }
            
        }
	}
}