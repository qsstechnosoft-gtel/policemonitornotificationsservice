using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Core;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.HostedServices;

namespace PoliceMonitorNotifications.ServiceHost.Logging
{
	public abstract class BaseLogService : IHostedService
	{
        public virtual void Initialize(XmlElement config, IEventManager eventManager, object param, bool testMode)
		{
			eventManager.Subscribe(typeof(LogMessage), LogMessageListener);
            eventManager.Subscribe(typeof(LogMessageDataInconsistancy), LogMessageInconsistancyListener);
			eventManager.Subscribe(typeof(ServiceHostState),ServiceHostStateListener);
		}

		private void LogMessageListener(object pubobj)
		{
			Log((LogMessage)pubobj);
		}
        private void LogMessageInconsistancyListener(object pubobj)
        {
            LogForDataInconsistancy((LogMessageDataInconsistancy)pubobj);
        }


		private void ServiceHostStateListener(object pubobj)
		{
			ServiceHostState state = (ServiceHostState) pubobj;
			switch (state)
			{
				case ServiceHostState.Startup:
					Startup();
					break;
				case ServiceHostState.Shutdown:
					Shutdown();
					break;
			}
		}		

		protected abstract void Log(LogMessage message);
        protected abstract void LogForDataInconsistancy(LogMessageDataInconsistancy message);
		protected virtual void Startup() { }
		protected virtual void Shutdown() { }

	}
}