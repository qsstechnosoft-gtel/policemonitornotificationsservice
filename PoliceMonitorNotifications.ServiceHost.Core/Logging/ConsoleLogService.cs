using System;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.Logging;
using PoliceMonitorNotifications.ServiceHost.Resources;
namespace PoliceMonitorNotifications.ServiceHost.Logging
{
	public class ConsoleLogService : BaseLogService
	{
		private LogMessage.SeverityType _severity = LogMessage.SeverityType.Info;
        private LogMessageDataInconsistancy.SeverityType _severityDataInconsistancy = LogMessageDataInconsistancy.SeverityType.Info;

		protected override void Log(LogMessage msg)
		{
            if (/*msg.Severity != LogMessage.SeverityType.Fail && */msg.Severity >= _severity)
			{
                Console.WriteLine(string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Debug_1"), msg.Severity, msg.Message));
				Exception ex = msg.Exception;
				while (ex != null)
				{
                    Console.WriteLine(string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Exception_1"), msg.Severity, ex.Message));
					ex = ex.InnerException;
				}
			}
		}

        protected override void LogForDataInconsistancy(LogMessageDataInconsistancy msg)
        {
            if (/*msg.Severity != LogMessage.SeverityType.Fail && */msg.Severity >= _severityDataInconsistancy)
            {
                Console.WriteLine(string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Debug_1"), msg.Severity, msg.Message));
                Exception ex = msg.Exception;
                while (ex != null)
                {
                    Console.WriteLine(string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Exception_1"), msg.Severity, ex.Message));
                    ex = ex.InnerException;
                }
            }
        }

        public override void Initialize(System.Xml.XmlElement config, IEventManager eventManager, object param, bool testMode)
		{
			base.Initialize(config, eventManager, param, false);

			if (config["LogLevel"] != null && ! string.IsNullOrEmpty(config["LogLevel"].InnerText))
			{
				string logLevel = config["LogLevel"].InnerText;

				try
				{
					_severity = (LogMessage.SeverityType) Enum.Parse(typeof(LogMessage.SeverityType), logLevel, true);
				}
				catch (Exception)
				{
                    Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Exception_2"));
				}
			}
		}

        protected override void Startup()
        {
            string version = null;
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                version = assembly.GetName().Version.ToString();
            }

            if (version != null)
            {
                Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Debug_2"), version);
            }
            else // version == null;
            {
                // the case of Unit Tests execution
                Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Debug_3"));
            }
        }		
		
		protected override void Shutdown()
		{
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleLogService_Debug_4"));
		}
	}
}