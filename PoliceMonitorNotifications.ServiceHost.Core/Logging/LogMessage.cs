using System;
using PoliceMonitorNotifications.ServiceHost.Eventing;

namespace PoliceMonitorNotifications.ServiceHost.Logging
{
	public class LogMessage
	{
		public enum SeverityType
		{
            Fail,
            Error,
            Debug,
			Info
		}

		public readonly SeverityType Severity;
		public readonly string Message;
		public readonly Exception Exception;
		public readonly DateTime Stamp;
        public readonly string ProjName;

        public readonly string V1ProjId;
        public readonly bool sendToUser = false;
        public readonly bool sendMail = true;
        public readonly string logMsg = string.Empty;
        public readonly bool sendToIS = false;

        private LogMessage(SeverityType severity, string message, Exception exception)
		{
			Severity = severity;
			Message = message;
			Exception = exception;
			Stamp = DateTime.Now;            
		}
        private LogMessage(SeverityType severity, string message, Exception exception, bool SendMail, bool SendToIS)
        {
            Severity = severity;
            Message = message;
            Exception = exception;
            Stamp = DateTime.Now;
            sendMail = SendMail;
            sendToIS = SendToIS;
        }
        private LogMessage(SeverityType severity, string message, Exception exception, string ProjectName)
        {
            Severity = severity;
            Message = message;
            Exception = exception;
            Stamp = DateTime.Now;
            ProjName = ProjectName;
        }
        private LogMessage(SeverityType severity, string message, Exception exception, string ProjectName, string V1ProjectId)
        {
            Severity = severity;
            Message = message;
            Exception = exception;
            Stamp = DateTime.Now;
            ProjName = ProjectName;
            V1ProjId = V1ProjectId;
        }
        private LogMessage(SeverityType severity, string message, Exception exception, string ProjectName, bool SendToUser, string LogMsg)
        {
            Severity = severity;
            Message = message;
            Exception = exception;
            Stamp = DateTime.Now;
            ProjName = ProjectName;
            sendToUser = SendToUser;
            logMsg = LogMsg;
        }
        private LogMessage(SeverityType severity, string message, Exception exception, string ProjectName, string V1ProjectId, bool SendToUser, string LogMsg)
        {
            Severity = severity;
            Message = message;
            Exception = exception;
            Stamp = DateTime.Now;
            ProjName = ProjectName;
            V1ProjId = V1ProjectId;
            sendToUser = SendToUser;
            logMsg = LogMsg;
        }

		public static void Log(string message, IEventManager eventmanager)
		{
			Log(SeverityType.Info, message, null, eventmanager);
		}

		public static void Log(string message, Exception exception, IEventManager eventmanager)
		{
            Log(SeverityType.Error, message, exception, eventmanager);
		}		
		
		public static void Log(SeverityType severity, string message, IEventManager eventmanager)
		{
			Log(severity, message, null, eventmanager);
		}
        public static void Log(SeverityType severity, string message, IEventManager eventmanager, bool SendMail,bool SendToIS)
        {
            Log(severity, message, null, eventmanager, SendMail, SendToIS);
        }
        
        public static void Log(string message, Exception exception, IEventManager eventmanager, string ProjectName)
        {
            Log(SeverityType.Error, message, exception, eventmanager, ProjectName);
        }

        public static void Log(string message, Exception exception, IEventManager eventmanager, string ProjectName, string V1ProjectId)
        {
            Log(SeverityType.Error, message, exception, eventmanager, ProjectName, V1ProjectId);
        }
               
        
        public static void Log(SeverityType severity, string message, Exception exception, IEventManager eventmanager)
		{
			eventmanager.Publish(new LogMessage(severity, message, exception));
		}
        public static void Log(SeverityType severity, string message, Exception exception, IEventManager eventmanager, bool SendMail, bool SendToIS)
        {
            eventmanager.Publish(new LogMessage(severity, message, exception, SendMail, SendToIS));
        }
        public static void Log(SeverityType severity, string message, Exception exception, IEventManager eventmanager, string ProjectName)
        {
            eventmanager.Publish(new LogMessage(severity, message, exception, ProjectName));
        }
        public static void Log(SeverityType severity, string message, Exception exception, IEventManager eventmanager, string ProjectName, string V1ProjectId)
        {
            eventmanager.Publish(new LogMessage(severity, message, exception, ProjectName,V1ProjectId));
        }

        public static void Log(SeverityType severity, string message, Exception exception, IEventManager eventmanager, string ProjectName, bool SendToUser, string LogMsg)
        {
            eventmanager.Publish(new LogMessage(severity, message, exception, ProjectName, SendToUser, LogMsg));
        }
        public static void Log(SeverityType severity, string message, Exception exception, IEventManager eventmanager, string ProjectName, string V1ProjectId, bool SendToUser, string LogMsg)
        {
            eventmanager.Publish(new LogMessage(severity, message, exception, ProjectName, V1ProjectId, SendToUser, LogMsg));
        }


        /*==============================================================================================================================*/

        public static void LogFailure(string message, Exception exception, IEventManager eventmanager)
        {
            Log(SeverityType.Fail, message, exception, eventmanager);
            //eventmanager.Publish(new LogMessage(SeverityType.Fail, message, exception));
        }

        public static void LogFailure(string message, IEventManager eventmanager)
        {
            LogFailure(message, null, eventmanager);
        }
      

        public static void LogFailure(string message, IEventManager eventmanager, string projectName)
        {
            LogFailure(message, null, eventmanager, projectName);
        }
        public static void LogFailure(string message, IEventManager eventmanager, string projectName, string V1ProjectId)
        {
            LogFailure(message, null, eventmanager, projectName, V1ProjectId);
        }
        public static void LogFailure(string message, IEventManager eventmanager, string projectName, bool SendTouser, string LogMsg)
        {
            LogFailure(message, null, eventmanager, projectName, SendTouser, LogMsg);
        }
        public static void LogFailure(string message, IEventManager eventmanager, string projectName, string V1ProjectId, bool SendTouser, string LogMsg)
        {
            LogFailure(message, null, eventmanager, projectName, V1ProjectId, SendTouser, LogMsg);
        }


        public static void LogFailure(string message, Exception exception, IEventManager eventmanager, string projectName)
        {
            Log(SeverityType.Fail, message, exception, eventmanager, projectName);             
        }
        public static void LogFailure(string message, Exception exception, IEventManager eventmanager, string projectName, string V1ProjectId)
        {
            Log(SeverityType.Fail, message, exception, eventmanager, projectName, V1ProjectId);
        }
        public static void LogFailure(string message, Exception exception, IEventManager eventmanager, string projectName, bool SendTouser, string LogMsg)
        {
            Log(SeverityType.Fail, message, exception, eventmanager, projectName, SendTouser, LogMsg);
        }
        public static void LogFailure(string message, Exception exception, IEventManager eventmanager, string projectName, string V1ProjectId, bool SendTouser, string LogMsg)
        {
            Log(SeverityType.Fail, message, exception, eventmanager, projectName, V1ProjectId, SendTouser, LogMsg);
        } 
	}

    public class LogMessageDataInconsistancy
	{
		public enum SeverityType
		{
            Fail,
            Error,
            Debug,
			Info
		}

		public readonly SeverityType Severity;
		public readonly string Message;
		public readonly Exception Exception;
		public readonly DateTime Stamp;
        public readonly string ProjName;

        public readonly string V1ProjId;

        private LogMessageDataInconsistancy(SeverityType severity, string message, string ProjectName, string V1ProjectId)
        {
            Severity = severity;
            Message = message;            
            Stamp = DateTime.Now;
            ProjName = ProjectName;
            V1ProjId = V1ProjectId;
        }
        private LogMessageDataInconsistancy(SeverityType severity, string message, string ProjectName )
        {
            Severity = severity;
            Message = message;
            Stamp = DateTime.Now;
            ProjName = ProjectName;            
        }

        public static void LogForDataInconsistancy(SeverityType severity, string message, string ProjectName, IEventManager eventmanager, string V1ProjectId)
        {
            eventmanager.Publish(new LogMessageDataInconsistancy(severity, message,ProjectName, V1ProjectId));
        }
        public static void LogForDataInconsistancy(SeverityType severity, string message, string ProjectName, IEventManager eventmanager)
        {
            eventmanager.Publish(new LogMessageDataInconsistancy(severity, message, ProjectName));
        }
    }
}