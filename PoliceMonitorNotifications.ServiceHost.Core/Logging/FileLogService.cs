using System;
using System.IO;
using System.Reflection;
using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.Logging;
using PoliceMonitorNotifications.ServiceHost.Resources;
namespace PoliceMonitorNotifications.ServiceHost.Logging
{
    public class FileLogService : BaseLogService
    {
        private const string _majorsep = "================================================================";
        private const string _minorsep = "----------------------------------------------------------------";

        private string _logFileName;
        private StreamWriter _writer = null;
        private string _logFailuresFileName;
        private StreamWriter _writerFailure = null;

        private static string lastMsg = null;

        private static XmlElement configuration = null;
        private static IEventManager eventMgr = null;
        private static int lastDayOfTheYear = -1;
        private static int lastYear = -1;
        private static string lastRegularLogName = null;
        private static string lastFailuresLogName = null;
        private static bool continued = false;
        private static object param;

        protected override void Log(LogMessage message)
        {
            //#if !DEBUG
            //            if (message.Severity == LogMessage.SeverityType.Debug)
            //                return;
            //#endif

            DateTime dtNow = DateTime.Now;
            if (dtNow.DayOfYear > lastDayOfTheYear || dtNow.Year > lastYear)
            {
                // Close the Current logs (Regular and Failures) and open new ones.
                continued = true;
                Shutdown();
                Initialize(configuration, eventMgr,param, false);
            }

            if (message == null)
            {
                return;
            }

            LogMessage.SeverityType severity = message.Severity;
            DateTime stamp = message.Stamp;
            string msg = String.IsNullOrEmpty(message.Message) ? "" : message.Message;
            string logMsg = String.IsNullOrEmpty(message.logMsg) ? "" : message.logMsg;
            Exception ex = message.Exception;
            if (message.sendToUser == false)
            {
                while (ex != null)
                {
                    if (!String.IsNullOrEmpty(ex.Message))
                    {
                        msg += msg == "" ? ex.Message : String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Exception_1"), ex.Message);
                    }
                    ex = ex.InnerException;
                }
                if (_writer != null)
                {
                    _writer.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_1"), severity, stamp, msg));
                }

            }
            else if (message.sendToUser == true && !string.IsNullOrEmpty(message.logMsg))
            {
                while (ex != null)
                {
                    if (!String.IsNullOrEmpty(ex.Message))
                    {
                        logMsg += logMsg == "" ? ex.Message : String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Exception_1"), ex.Message);
                    }
                    ex = ex.InnerException;
                }
                if (_writer != null)
                {
                    _writer.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_1"), severity, stamp, logMsg));
                }

            }
            
            if (severity == LogMessage.SeverityType.Fail && _writerFailure != null)
            {
                if (message.sendToUser == false)
                {
                    _writerFailure.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_2"), stamp, msg));
                }
                else if (message.sendToUser == true && !string.IsNullOrEmpty(message.logMsg))
                {
                    _writerFailure.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_2"), stamp, logMsg));
                }
                // To avoid spam
                if (lastMsg == null || lastMsg.CompareTo(msg) != 0)
                {
                    try
                    {                        
                        if (message.sendMail == true)
                        {
                            if (!string.IsNullOrEmpty(message.ProjName) && !string.IsNullOrEmpty(message.V1ProjId))
                            {
                                //if (message.sendToUser == true)
                                //{
                                //    Mailing.MailService.SendMail(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_3"), severity, stamp, msg), message.ProjName, message.V1ProjId);
                                //}
                                //else
                                //{
                                //    Mailing.MailService.SendMail(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_3"), severity, stamp, msg), message.ProjName, message.V1ProjId, false,message.sendToIS);
                                //}

                            }
                            else if (!string.IsNullOrEmpty(message.ProjName))
                            {
                                //if (message.sendToUser == true)
                                //{
                                //    Mailing.MailService.SendMail(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_4"), severity, stamp, msg), message.ProjName);
                                //}
                                //else
                                //{
                                //    Mailing.MailService.SendMail(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_4"), severity, stamp, msg), message.ProjName, false, message.sendToIS);
                                //}
                            }
                            else
                            {
                                Mailing.MailService.SendMail(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_5"), severity, stamp, msg), message.sendToIS);
                            }
                        }
                    }
                    catch (Exception e)
                    {                        
                        if (_writer != null) _writer.WriteLine(string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Fail_1"), e.Message));                        
                        if (_writerFailure != null) _writerFailure.WriteLine(string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Fail_2"), e.Message));
                    }
                }

                lastMsg = msg;
            }
        }

        protected override void LogForDataInconsistancy(LogMessageDataInconsistancy message)
        {
            DateTime dtNow = DateTime.Now;
            if (dtNow.DayOfYear > lastDayOfTheYear || dtNow.Year > lastYear)
            {
                // Close the Current logs (Regular and Failures) and open new ones.
                continued = true;
                Shutdown();
                Initialize(configuration, eventMgr, param, false);
            }

            if (message == null)
            {
                return;
            }

            LogMessageDataInconsistancy.SeverityType severity = message.Severity;
            DateTime stamp = message.Stamp;
            string msg = String.IsNullOrEmpty(message.Message) ? "" : message.Message;
            Exception ex = message.Exception;
            while (ex != null)
            {
                if (!String.IsNullOrEmpty(ex.Message))
                {
                    msg += msg == "" ? ex.Message : String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Exception_1"), ex.Message);
                }
                ex = ex.InnerException;
            }

            //if (_writer != null)
            //{
            //    _writerDataInconsistancy.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_1"), severity, stamp, msg));
            //}
        }

        public override void Initialize(XmlElement config, IEventManager eventManager, object _param, bool testMode)
        {
            DateTime dtNow = DateTime.Now;
            if (lastDayOfTheYear == lastYear)
            {
                base.Initialize(config, eventManager, param, false);
            }

            configuration = config;
            eventMgr = eventManager;
            param = _param;
           
            
            string postfix = String.Format("_{0}_{1}.log", String.Format("{0:yyyy-MM-dd}", dtNow), String.Format("{0:HH.mm.ss}", dtNow));
            _logFileName = config["LogFile"].InnerText + postfix;
            _logFailuresFileName = config["LogFailuresFile"].InnerText + postfix;

            string folder = Path.GetDirectoryName(_logFileName);
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            string version = null;
            Assembly assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                version = assembly.GetName().Version.ToString();
            }

            // Regular Log
            _writer = new StreamWriter(_logFileName, true);
            _writer.AutoFlush = true;
            
            // Failures Log
            _writerFailure = new StreamWriter(_logFailuresFileName, true);
            _writerFailure.AutoFlush = true;


            if (version != null)
            {
                if (lastRegularLogName == null)
                {
                    _writer.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_6"), version);
                }
                else // lastRegularLogName != null
                {
                    _writer.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_7"), lastRegularLogName);
                }
                if (lastFailuresLogName == null)
                {
                    _writerFailure.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_8"), version);
                }
                else // lastFailuresLogName != null
                {
                    _writerFailure.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_9"), lastFailuresLogName);
                }
            }
            else // version == null - the case of Unit Tests execution
            {
                if (lastRegularLogName == null)
                {
                    _writer.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_10"));
                }
                else // lastRegularLogName != null
                {
                    _writer.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_11"), lastRegularLogName);
                }
                if (lastFailuresLogName == null)
                {
                    _writerFailure.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_12"));
                }
                else // lastFailuresLogName != null
                {
                    _writerFailure.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_13"), lastFailuresLogName);
                }
            }
            _writer.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_14"), dtNow /*DateTime.Now*/);
            _writer.WriteLine(_majorsep);
            _writerFailure.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_15"), dtNow /*DateTime.Now*/);
            _writerFailure.WriteLine(_majorsep);
            
            lastDayOfTheYear = dtNow.DayOfYear;
            lastYear = dtNow.Year;
            lastRegularLogName = _logFileName;
            lastFailuresLogName = _logFailuresFileName;
        }

        protected override void Shutdown()
        {
            DateTime dtNow = DateTime.Now;
            if (_writer != null)
            {
                _writer.WriteLine(_majorsep);
                if (continued)
                {
                    _writer.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_16"), dtNow);
                }
                else // continued == false
                {
                    _writer.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Debug_17"), dtNow);
                }
                _writer.Dispose();
                _writer.Close();
                _writer = null;
            }

            if (_writerFailure != null)
            {
                _writerFailure.WriteLine(_majorsep);
                if (continued)
                {
                    _writerFailure.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Fail_3"), dtNow);
                }
                else // continued == false
                {
                    _writerFailure.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_FileLogService_Fail_4"), dtNow);
                }
                _writerFailure.Dispose();
                _writerFailure.Close();
                _writerFailure = null;
            }
           
            continued = false;

            base.Shutdown();
        }
    }
}