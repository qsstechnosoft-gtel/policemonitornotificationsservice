using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Eventing;

namespace PoliceMonitorNotifications.ServiceHost.HostedServices
{
	public interface IHostedService
	{
		void Initialize(XmlElement config, IEventManager eventManager, object param, bool testMode);
    }

    public interface IHostedServiceTest : IHostedService
    {
    }
}