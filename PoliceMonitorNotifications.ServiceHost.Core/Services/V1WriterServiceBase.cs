using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.HostedServices;

namespace PoliceMonitorNotifications.ServiceHost.Core.Services
{
    public abstract class V1WriterServiceBase : IHostedService
	{

		protected XmlElement _config;
		protected IEventManager _eventManager;
        
		

        public virtual void Initialize(XmlElement config, IEventManager eventManager, object param, bool testMode)
		{
			_config = config;
			_eventManager = eventManager;
		}

		protected abstract NeededAssetType[] NeededAssetTypes { get; }

		
		protected virtual void VerifyRuntimeMeta() { }

		protected struct NeededAssetType
		{
			public readonly string Name;
			public readonly string[] AttributeDefinitionNames;
			public NeededAssetType (string name, string[] attributedefinitionnames)
			{
				Name = name;
				AttributeDefinitionNames = attributedefinitionnames;
			}
		}

	}
}
