using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using PoliceMonitorNotifications.ServiceHost.HostedServices;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
	public class ServicesConfigurationHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			return new ServicesConfiguration(section);
		}
	}

	public class ServiceInfo
	{
		public readonly string Name;
		public readonly XmlElement Config;
		public readonly IHostedService Service;

		public ServiceInfo(string name, IHostedService svc, XmlElement config)
		{
			Name = name;
			Service = svc;
			Config = config;
		}

	}
	
	internal class ServicesConfiguration : List<ServiceInfo>
	{
		public ServicesConfiguration(XmlNode section)
		{
			foreach (XmlNode child in section.ChildNodes)
			{
				if (child.NodeType != XmlNodeType.Element)
					continue;

				XmlAttribute disabledAttribute = child.Attributes["disabled"];
				if (disabledAttribute != null)
				{
                    if (disabledAttribute.Value.ToLower() == "yes" || disabledAttribute.Value.ToLower() == "true" || disabledAttribute.Value == "1")
					{
						continue;
					}
				}

				XmlAttribute attrib = child.Attributes["class"];
				if (attrib == null)
					continue;

				Type type = Type.GetType(attrib.Value);

				try
				{
					IHostedService svc = (IHostedService)Activator.CreateInstance(type);

                    if (child.LocalName == "MailService")
                    {
                        XmlAttribute _attrib = child.Attributes["refMailService"];
                        if (_attrib != null)
                        {
                            XmlDocument _xmlDocument = new XmlDocument();
                            _xmlDocument.Load(_attrib.Value);

                            XmlNode node = _xmlDocument.SelectSingleNode("/configuration/Services/MailService");
                            ((XmlElement)node)["Environment"].InnerText = ((XmlElement)child)["Environment"].InnerText;
                            if (node != null)
                            {
                                Add(new ServiceInfo(child.LocalName, svc, (XmlElement)node));
                            }
                        }
                        else
                        {
                            Add(new ServiceInfo(child.LocalName, svc, (XmlElement)child));
                        }
                    }
                    else
                    {
                        Add(new ServiceInfo(child.LocalName, svc, (XmlElement)child));
                    }

                    Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServicesConfiguration_Debug_1"), attrib.Value);
				}
				catch (Exception ex)
				{
                    Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServicesConfiguration_Exception_1"), attrib.Value, ex);					
				}
			}

		}
	}
}