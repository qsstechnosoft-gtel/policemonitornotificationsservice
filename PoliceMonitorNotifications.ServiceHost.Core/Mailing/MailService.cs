﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Security.Cryptography;
using System.IO;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.HostedServices;
using PoliceMonitorNotifications.ServiceHost.Core.Utility;
using PoliceMonitorNotifications.ServiceHost.Logging;
using System.Net.Mime;

namespace PoliceMonitorNotifications.ServiceHost.Mailing
{
    public class MailService : IHostedService
    {
        public static System.Xml.XmlElement Config;
        public void Initialize(System.Xml.XmlElement config, IEventManager eventManager, object param, bool testMode)
        {
            string server = config["SMTPServer"].InnerText;
            string[] credentials = config["Credentials"].InnerText.Split(',');
            string name = credentials[0].Trim();
            string password = string.Empty;
            string originalPassword = credentials[1].Trim();

            int? portNumber = Converters.ToNullable<int>(config["Port"].InnerText);
            bool enableSSL = Convert.ToBoolean(config["EnableSSL"].InnerText);

            password = originalPassword;
            //string passPhrase = "Pas5pr@se";        // can be any string
            //string saltValue = "s@1tValue";        // can be any string
            //string hashAlgorithm = "SHA1";        // can be any string
            //int passwordIterations = 2;        // can be any string
            //string initVector = "@1B2c3D4e5F6g7H8";
            //int keySize = 256;

            //try
            //{
            //    password = RijndaelSimple.Decrypt(originalPassword,
            //                                                passPhrase,
            //                                                saltValue,
            //                                                hashAlgorithm,
            //                                                passwordIterations,
            //                                                initVector,
            //                                                keySize);
            //}
            //catch (Exception e)
            //{
            //    Logging.LogMessage.Log(Logging.LogMessage.SeverityType.Fail, "Error decoding the password. Connector will stop. Reason:", e, eventManager);
            //    Environment.Exit(-6);
            //}
            from = config["From"].InnerText;
            to_default = config["To"].InnerText;
            cc_default = config["Cc"].InnerText;
            subject_default = config["Subject"].InnerText;
         

            environment_default = config["Environment"].InnerText;
            environment_default = " " + environment_default + " ";

            subject_default = environment_default + " " + subject_default;

            if (portNumber!=null)
                client = new SmtpClient(server, Convert.ToInt32(portNumber));
            else
                client = new SmtpClient(server);

            client.Credentials = new System.Net.NetworkCredential(name, password);
            client.EnableSsl = enableSSL;

            MailService.SMTPServer = server;
            MailService.SMTPPort = portNumber;
            MailService.UserName=name;
            MailService.Password = password;
            MailService.EnableSSL = enableSSL;


            Config = config;
        }

        public static string SMTPServer
        {
            get { return server; }
            set { server = value; }
        }
        public static int? SMTPPort
        {
            get { return portNumber; }
            set { portNumber = value; }
        }
        public static string UserName
        {
            get { return username; }
            set { username = value; }
        }
        public static string Password
        {
            get { return password; }
            set { password = value; }
        }
        public static bool EnableSSL
        {
            get { return enableSSL; }
            set { enableSSL = value; }
        }



        #region TO_Default property
        public static string TO_Default
        {
            get { return to_default; }
            
        }
        #endregion

         #region CC_Default property
        public static string CC_Default
        {
            get { return cc_default; }
  
        }
        #endregion

       

        public static bool IsEnabled
        {
            get { return client != null; }
        }

        /// <summary>
        /// Send Mail using the predefined (from Config file) To, Cc, Subject.
        /// </summary>
        /// <param name="body"></param>
        public static void SendMail(string body)
        {
            if (!IsEnabled || String.IsNullOrEmpty(body))
            {
                return; // Mail is disabled, or empty Body
            }
            if (!String.IsNullOrEmpty(subject_default))
            {
                subject_default = subject_default.Replace('\r', ' ').Replace('\n', ' ');
            }
            mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from);            
            mailMessage.Subject = subject_default;
            mailMessage.Body = body;

            // Allow multiple "To" addresses to be separated by a semi-colon
            if (to_default.Trim().Length > 0)
            {
                  if (!to_default.Contains(";"))
                    to_default = to_default + ";";

                    foreach (string addr in to_default.Split(';'))
                    {
                        if (!String.IsNullOrEmpty(addr))
                        {
                            try
                            {
                                mailMessage.To.Add(new MailAddress(addr.Trim()));
                            }
                            catch (FormatException ex)
                            {
                                throw new EmailException(ex.Message, ex);
                            }
                        }
                    }
              
            }

            // Allow multiple "Cc" addresses to be separated by a semi-colon
            if (cc_default.Trim().Length > 0)
            {
                 if (!cc_default.Contains(";"))
                    cc_default = cc_default + ";";

                    foreach (string addr in cc_default.Split(';'))
                    {
                        if (!String.IsNullOrEmpty(addr))
                        {
                            try
                            {
                                mailMessage.CC.Add(new MailAddress(addr.Trim()));
                            }
                            catch (FormatException ex)
                            {
                                throw new EmailException(ex.Message, ex);
                            }
                        }
                    }
                
            }
            mailMessage.IsBodyHtml = true;
            try
            {
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw new EmailException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Send Mail using the predefined (from Config file) To, Cc, Subject.
        /// </summary>
        /// <param name="body"></param>
        public static void SendMail(string body,bool SendToIs)
        {
            if (!IsEnabled || String.IsNullOrEmpty(body))
            {
                return; // Mail is disabled, or empty Body
            }
            if (!String.IsNullOrEmpty(subject_default))
            {
                subject_default = subject_default.Replace('\r', ' ').Replace('\n', ' ');
            }
            mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from);
            mailMessage.Subject = subject_default;
            mailMessage.Body = body;

            // Allow multiple "To" addresses to be separated by a semi-colon
            if (to_default.Trim().Length > 0)
            {
                if (!to_default.Contains(";"))
                    to_default = to_default + ";";

                foreach (string addr in to_default.Split(';'))
                {
                    if (!String.IsNullOrEmpty(addr))
                    {
                        try
                        {
                            mailMessage.To.Add(new MailAddress(addr.Trim()));
                        }
                        catch (FormatException ex)
                        {
                            throw new EmailException(ex.Message, ex);
                        }
                    }
                }

            }

            // Allow multiple "Cc" addresses to be separated by a semi-colon
            if (cc_default.Trim().Length > 0)
            {
                if (!cc_default.Contains(";"))
                    cc_default = cc_default + ";";

                foreach (string addr in cc_default.Split(';'))
                {
                    if (!String.IsNullOrEmpty(addr))
                    {
                        try
                        {
                            mailMessage.CC.Add(new MailAddress(addr.Trim()));
                        }
                        catch (FormatException ex)
                        {
                            throw new EmailException(ex.Message, ex);
                        }
                    }
                }

            }
            mailMessage.IsBodyHtml = true;
            try
            {
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw new EmailException(ex.Message, ex);
            }
            if (SendToIs == true)
            {
                string ToList = string.Empty;
                string CCList = string.Empty;
                string subject = string.Empty;
                System.Xml.XmlNodeList EmailNotificationForStoppedConnector = Config.GetElementsByTagName("EmailNotificationForISTeam");

                if (EmailNotificationForStoppedConnector.Count == 0)
                {
                    return;
                }
                ToList = EmailNotificationForStoppedConnector[0].ChildNodes[0].InnerText;
                CCList = EmailNotificationForStoppedConnector[0].ChildNodes[1].InnerText;
                subject = EmailNotificationForStoppedConnector[0].ChildNodes[2].InnerText;


                if (!String.IsNullOrEmpty(subject))
                {
                    subject = environment_default + " " + subject;
                    subject = subject.Replace('\r', ' ').Replace('\n', ' ');
                }
                mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(from);
                mailMessage.Subject = subject;
                mailMessage.Body = body;

                // Allow multiple "To" addresses to be separated by a semi-colon
                if (ToList.Trim().Length > 0)
                {
                    if (!ToList.Contains(";"))
                        ToList = ToList + ";";

                    foreach (string addr in ToList.Split(';'))
                    {
                        if (!String.IsNullOrEmpty(addr))
                        {
                            try
                            {
                                mailMessage.To.Add(new MailAddress(addr.Trim()));
                            }
                            catch (FormatException ex)
                            {
                                throw new EmailException(ex.Message, ex);
                            }
                        }
                    }

                }

                // Allow multiple "Cc" addresses to be separated by a semi-colon
                if (CCList.Trim().Length > 0)
                {
                    if (!CCList.Contains(";"))
                        CCList = CCList + ";";

                    foreach (string addr in CCList.Split(';'))
                    {
                        if (!String.IsNullOrEmpty(addr))
                        {
                            try
                            {
                                mailMessage.CC.Add(new MailAddress(addr.Trim()));
                            }
                            catch (FormatException ex)
                            {
                                throw new EmailException(ex.Message, ex);
                            }
                        }
                    }

                }
                mailMessage.IsBodyHtml = true;
                try
                {
                    if (ToList.Trim().Length > 0 || CCList.Trim().Length > 0)
                        client.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    throw new EmailException(ex.Message, ex);
                }
            }
        }


        /// <summary>
        /// Send Mail using custom To, Cc, Subject and Body.
        /// </summary>
        /// <param name="toRecipients"></param>
        /// <param name="ccRecipients"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>

        public static void SendMailAsync(string[] toRecipients, string[] ccRecipients, string subject, string body, byte[] fileByteArray) 
        {
            if (!IsEnabled)
            {
                return; // Mail is disabled
            }
            if (((toRecipients == null || toRecipients.Length == 0)))
            { return; }
            try
            {
                //string defaults = DefaultMail + "" + DefaultPass;
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("Smtp.office365.com");
                mail.From = new MailAddress("alerts@radio1033.com");
                //mail.To.Add("");

                if (toRecipients != null)
                {
                    foreach (string toRecipient in toRecipients)
                    {
                        if (!String.IsNullOrEmpty(toRecipient))
                        {
                            try
                            {
                                mail.Bcc.Add(new MailAddress(toRecipient.Trim()));
                            }
                            catch (FormatException ex)
                            {
                                throw new EmailException(ex.Message, ex);
                            }
                        }
                    }
                }
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;
                Attachment att = new Attachment(new MemoryStream(fileByteArray), System.Net.Mime.MediaTypeNames.Application.Octet);
                att.Name = "Mp3File.mp3";
                mail.Attachments.Add(att);
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("alerts@radio1033.com", "Radio1033rocks!");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw new EmailException(ex.Message, ex);
            }
        }

        public static void SendMail(string[] toRecipients, string[] ccRecipients, string subject, string body, byte[] fileByteArray)
        //public static void SendMail(string[] toRecipients, string[] ccRecipients, string subject, string body)
        {
            ////if (!IsEnabled)
            ////{
            ////    return; // Mail is disabled
            ////}
            ////if (((toRecipients == null || toRecipients.Length == 0)
            ////    && (ccRecipients == null || ccRecipients.Length == 0))
            ////    || (String.IsNullOrEmpty(subject) && String.IsNullOrEmpty(body)))
            ////{
            ////    return;
            ////}

            //if (((toRecipients == null || toRecipients.Length == 0)))
            //{ return; }

            //mailMessage = new MailMessage();
            //mailMessage.From = new MailAddress(from);
            //mailMessage.Subject = subject;
            //mailMessage.Body = body;

            //if (toRecipients != null)
            //{
            //    foreach (string toRecipient in toRecipients)
            //    {
            //        if (!String.IsNullOrEmpty(toRecipient))
            //        {
            //            try
            //            {
            //                mailMessage.To.Add(new MailAddress(toRecipient.Trim()));
            //            }
            //            catch(FormatException ex)
            //            {
            //                throw new EmailException(ex.Message,ex);
            //            }
            //        }
            //    }
            //}
            //if (ccRecipients != null)
            //{
            //    foreach (string ccRecipient in ccRecipients)
            //    {
            //        if (!String.IsNullOrEmpty(ccRecipient))
            //        {
            //            try
            //            {
            //                mailMessage.CC.Add(new MailAddress(ccRecipient.Trim()));
            //            }
            //            catch (FormatException ex)
            //            {
            //                throw new EmailException(ex.Message,ex);
            //            }
            //        }
            //    }
            //}
            //mailMessage.IsBodyHtml = true;
            //try
            //{
            //    if (!String.IsNullOrEmpty(mailMessage.Subject))
            //    {
            //        mailMessage.Subject = mailMessage.Subject.Replace('\r', ' ').Replace('\n', ' '); 
            //    }
            //    client.Send(mailMessage);
            //}
            //catch (Exception ex)
            //{
            //    throw new EmailException(ex.Message, ex);
            //}




            if (!IsEnabled)
            {
                return; // Mail is disabled
            }
            if (((toRecipients == null || toRecipients.Length == 0)))
            { return; }
            try
            {
                //string defaults = DefaultMail + "" + DefaultPass;
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("Smtp.office365.com");
                mail.From = new MailAddress("alerts@radio1033.com");
                //mail.To.Add("");

                if (toRecipients != null)
                {
                    foreach (string toRecipient in toRecipients)
                    {
                        if (!String.IsNullOrEmpty(toRecipient))
                        {
                            try
                            {
                                mail.To.Add(new MailAddress(toRecipient.Trim()));
                            }
                            catch (FormatException ex)
                            {
                                throw new EmailException(ex.Message, ex);
                            }
                        }
                    }
                }    
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;
                Attachment att = new Attachment(new MemoryStream(fileByteArray), System.Net.Mime.MediaTypeNames.Application.Octet);
                att.Name = "Mp3File.mp3";
                mail.Attachments.Add(att);
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("alerts@radio1033.com", "Radio1033rocks!");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw new EmailException(ex.Message, ex);
            }
        }
        
       

        //public static async System.Threading.Tasks.Task SendMailAsync(string[] toRecipients, string[] ccRecipients, string subject, string body, byte[] fileByteArray)
        //{

        //    if (!IsEnabled)
        //    {
        //        return; // Mail is disabled
        //    }
        //    //if (MailService.SMTPPort != null)
        //    //    client = new SmtpClient(MailService.SMTPServer, Convert.ToInt32(MailService.SMTPPort));
        //    //else
        //    //    client = new SmtpClient(MailService.SMTPServer);

        //    //client.UseDefaultCredentials = false;// new System.Net.NetworkCredential(MailService.UserName, MailService.Password);
        //    //client.EnableSsl = MailService.EnableSSL;

        //    //if (((toRecipients == null || toRecipients.Length == 0)
        //    //    && (ccRecipients == null || ccRecipients.Length == 0))
        //    //    || (String.IsNullOrEmpty(subject) && String.IsNullOrEmpty(body)))
        //    //{
        //    //    return;
        //    //}

        //    if (((toRecipients == null || toRecipients.Length == 0)))
        //    { return; }


        //    //mailMessage = new MailMessage();
        //    //mailMessage.From = new MailAddress(from);
        //    //mailMessage.Subject = subject;
        //    //mailMessage.Body = body;
        //    //mailMessage.To.Add(" ");
        //    //if (toRecipients != null)
        //    //{
        //    //    foreach (string toRecipient in toRecipients)
        //    //    {
        //    //        if (!String.IsNullOrEmpty(toRecipient))
        //    //        {
        //    //            try
        //    //            {
        //    //                mailMessage.To.Add(new MailAddress(toRecipient.Trim()));
        //    //            }
        //    //            catch (FormatException ex)
        //    //            {
        //    //                throw new EmailException(ex.Message, ex);
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    //if (ccRecipients != null)
        //    //{
        //    //    foreach (string ccRecipient in ccRecipients)
        //    //    {
        //    //        if (!String.IsNullOrEmpty(ccRecipient))
        //    //        {
        //    //            try
        //    //            {
        //    //                mailMessage.CC.Add(new MailAddress(ccRecipient.Trim()));
        //    //            }
        //    //            catch (FormatException ex)
        //    //            {
        //    //                throw new EmailException(ex.Message, ex);
        //    //            }
        //    //        }
        //    //    }
        //    //}

        //    try
        //    {
        //        MailMessage mail = new MailMessage();
        //        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

        //        //mail.From = new MailAddress(from);

        //        mail.From = new MailAddress("");

        //        //if (toRecipients != null)
        //        //{
        //        //    foreach (string toRecipient in toRecipients)
        //        //    {
        //        //        if (!String.IsNullOrEmpty(toRecipient))
        //        //        {
        //        //            try
        //        //            {
        //        //                mail.To.Add(new MailAddress(toRecipient.Trim()));
        //        //            }
        //        //            catch (FormatException ex)
        //        //            {
        //        //                throw new EmailException(ex.Message, ex);
        //        //            }
        //        //        }
        //        //    }
        //        //}



        //        mail.To.Add("");
        //        mail.Subject = subject;
        //        mail.IsBodyHtml = true;
        //        mail.Body = body;
        //        Attachment att = new Attachment(new MemoryStream(fileByteArray), System.Net.Mime.MediaTypeNames.Application.Octet);
        //        att.Name = "Mp3File.mp3";
        //        mail.Attachments.Add(att);
        //        SmtpServer.Port = 587;
        //        SmtpServer.Credentials = new System.Net.NetworkCredential("", "");
        //        SmtpServer.EnableSsl = true;

        //        SmtpServer.Send(mail);
        //    }






        //    //mailMessage.IsBodyHtml = true;
        //    //try
        //    //{
        //    //    if (!String.IsNullOrEmpty(mailMessage.Subject))
        //    //    {
        //    //        mailMessage.Subject = mailMessage.Subject.Replace('\r', ' ').Replace('\n', ' ');
        //    //    }
        //    //    Attachment att = new Attachment(new MemoryStream(fileByteArray), System.Net.Mime.MediaTypeNames.Application.Octet);
        //    //    att.Name = "Mp3File.mp3";
        //    //    //ContentType content = att.ContentType;
        //    //    //content.MediaType = MediaTypeNames.Text.;

        //    //    mailMessage.Attachments.Add(att);
        //    //    await client.SendMailAsync(mailMessage);
        //    // }
        //    catch (Exception ex)
        //    {
        //        throw new EmailException(ex.Message, ex);
        //    }
        //}

        /// <summary>
        /// Return the array of "To" and "Cc" list
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static string[] GetEmailAddressArray(string emailAddress)
        {
            if (emailAddress.Trim().Length > 0)
            {
                if (!emailAddress.Contains(";"))
                    emailAddress = emailAddress + ";";

            }
            return emailAddress.Split(';');
        }

        private static SmtpClient client = null;
        private static MailMessage mailMessage = null;
        private static string from = "alerts@radio1033.com";
        private static string DefaultMail = "alerts@gtel.tech";
        private static string DefaultPass = "qsssoftware1!";
        private static string environment_default = null;
        private static string subject_default = null;

        private static string to_default = null;
        private static string cc_default = null;

        private static string server = null;
        private static int? portNumber = null;
        private static string username = null;
        private static string password = null;
        private static bool enableSSL = false;

         

         
    }

    #region RijndaelSimple
    public class RijndaelSimple
    {
        /// <summary>
        /// Encrypts specified plaintext using Rijndael symmetric key algorithm
        /// and returns a base64-encoded result.
        /// </summary>
        /// <param name="plainText">
        /// Plaintext value to be encrypted.
        /// </param>
        /// <param name="passPhrase">
        /// Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key.
        /// Passphrase can be any string. In this example we assume that this
        /// passphrase is an ASCII string.
        /// </param>
        /// <param name="saltValue">
        /// Salt value used along with passphrase to generate password. Salt can
        /// be any string. In this example we assume that salt is an ASCII string.
        /// </param>
        /// <param name="hashAlgorithm">
        /// Hash algorithm used to generate password. Allowed values are: "MD5" and
        /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        /// </param>
        /// <param name="passwordIterations">
        /// Number of iterations used to generate password. One or two iterations
        /// should be enough.
        /// </param>
        /// <param name="initVector">
        /// Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be 
        /// exactly 16 ASCII characters long.
        /// </param>
        /// <param name="keySize">
        /// Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
        /// Longer keys are more secure than shorter keys.
        /// </param>
        /// <returns>
        /// Encrypted value formatted as a base64-encoded string.
        /// </returns>
        public static string Encrypt(string plainText,
                                     string passPhrase,
                                     string saltValue,
                                     string hashAlgorithm,
                                     int passwordIterations,
                                     string initVector,
                                     int keySize)
        {
            // Convert strings into byte arrays.
            // Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
            // encoding.
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            // Convert our plaintext into a byte array.
            // Let us assume that plaintext contains UTF8-encoded characters.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // First, we must create a password, from which the key will be derived.
            // This password will be generated from the specified passphrase and 
            // salt value. The password will be created using the specified hash 
            // algorithm. Password creation can be done in several iterations.
            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                            passPhrase,
                                                            saltValueBytes,
                                                            hashAlgorithm,
                                                            passwordIterations);

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
            byte[] keyBytes = password.GetBytes(keySize / 8);

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC;

            // Generate encryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                                                             keyBytes,
                                                             initVectorBytes);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream();

            // Define cryptographic stream (always use Write mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         encryptor,
                                                         CryptoStreamMode.Write);
            // Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            // Finish encrypting.
            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes = memoryStream.ToArray();

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.
            string cipherText = Convert.ToBase64String(cipherTextBytes);

            // Return encrypted string.
            return cipherText;
        }

        /// <summary>
        /// Decrypts specified ciphertext using Rijndael symmetric key algorithm.
        /// </summary>
        /// <param name="cipherText">
        /// Base64-formatted ciphertext value.
        /// </param>
        /// <param name="passPhrase">
        /// Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key.
        /// Passphrase can be any string. In this example we assume that this
        /// passphrase is an ASCII string.
        /// </param>
        /// <param name="saltValue">
        /// Salt value used along with passphrase to generate password. Salt can
        /// be any string. In this example we assume that salt is an ASCII string.
        /// </param>
        /// <param name="hashAlgorithm">
        /// Hash algorithm used to generate password. Allowed values are: "MD5" and
        /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        /// </param>
        /// <param name="passwordIterations">
        /// Number of iterations used to generate password. One or two iterations
        /// should be enough.
        /// </param>
        /// <param name="initVector">
        /// Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be
        /// exactly 16 ASCII characters long.
        /// </param>
        /// <param name="keySize">
        /// Size of encryption key in bits. Allowed values are: 128, 192, and 256.
        /// Longer keys are more secure than shorter keys.
        /// </param>
        /// <returns>
        /// Decrypted string value.
        /// </returns>
        /// <remarks>
        /// Most of the logic in this function is similar to the Encrypt
        /// logic. In order for decryption to work, all parameters of this function
        /// - except cipherText value - must match the corresponding parameters of
        /// the Encrypt function which was called to generate the
        /// ciphertext.
        /// </remarks>
        public static string Decrypt(string cipherText,
                                     string passPhrase,
                                     string saltValue,
                                     string hashAlgorithm,
                                     int passwordIterations,
                                     string initVector,
                                     int keySize)
        {
            // Convert strings defining encryption key characteristics into byte
            // arrays. Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8
            // encoding.
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            // Convert our ciphertext into a byte array.
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            // First, we must create a password, from which the key will be 
            // derived. This password will be generated from the specified 
            // passphrase and salt value. The password will be created using
            // the specified hash algorithm. Password creation can be done in
            // several iterations.
            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                            passPhrase,
                                                            saltValueBytes,
                                                            hashAlgorithm,
                                                            passwordIterations);

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
            byte[] keyBytes = password.GetBytes(keySize / 8);

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC;

            // Generate decryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                                                             keyBytes,
                                                             initVectorBytes);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

            // Define cryptographic stream (always use Read mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                          decryptor,
                                                          CryptoStreamMode.Read);

            // Since at this point we don't know what the size of decrypted data
            // will be, allocate the buffer long enough to hold ciphertext;
            // plaintext is never longer than ciphertext.
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            // Start decrypting.
            int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                       0,
                                                       plainTextBytes.Length);

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert decrypted data into a string. 
            // Let us assume that the original plaintext string was UTF8-encoded.
            string plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                       0,
                                                       decryptedByteCount);

            // Return decrypted string.   
            return plainText;
        }
    }
    #endregion

}