﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoliceMonitorNotifications.ServiceHost.Mailing
{
    public class EmailException :  ApplicationException 
    {
        public EmailException(string message) : base(message) { }
        public EmailException(string message, Exception inner) : base(message, inner) { }
    }
}
