namespace PoliceMonitorNotifications.ServiceHost.Core
{
	public enum ServiceHostState
	{
		Startup,
		Shutdown
	}
}