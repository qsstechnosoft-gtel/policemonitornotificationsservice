using System;

namespace PoliceMonitorNotifications.ServiceHost.Eventing
{
	public delegate void EventDelegate(object pubobj);
    public delegate void RestartRequired();
	
	public interface IEventManager
	{
		bool Publish(object pubobj);
		void Subscribe(Type pubtype, EventDelegate listener);
        event RestartRequired OnRestartRequired;
	}
}