using System;
using System.Collections.Generic;
using PoliceMonitorNotifications.ServiceHost.Core;
using PoliceMonitorNotifications.ServiceHost.Logging;
using PoliceMonitorNotifications.ServiceHost.Mailing;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost.Eventing
{
	public class EventManager : IEventManager
	{
        public EventManager()
        {
        }
		IDictionary<Type, EventDelegate> _subscriptions = new Dictionary<Type, EventDelegate>();
        bool completed = false;
        List<string> publishedEvents = new List<string>();

        public bool Publish(object pubobj)
        {
                EventDelegate subs;
                if (_subscriptions.TryGetValue(pubobj.GetType(), out subs))
                {
                    lock (this)
                    {
                        subs(pubobj);
                    }
                }

                return completed;
        }

		public void Subscribe(Type pubtype, EventDelegate listener)
		{
			EventDelegate subs;
			EventDelegate newlistener = WrapListener(listener);
			if (!_subscriptions.TryGetValue(pubtype, out subs))
				_subscriptions[pubtype] = newlistener;
			else
				_subscriptions[pubtype] = (EventDelegate)Delegate.Combine(subs, newlistener);
		}

        public event RestartRequired OnRestartRequired;

		private EventDelegate WrapListener(EventDelegate listener)
		{
			return delegate(object pubobj) 
			       	{
						try
                        {
                            if (publishedEvents.Contains(pubobj.ToString()))
                            {
                                completed = false;
                            }                            
                            listener(pubobj);
                            if ( publishedEvents.Contains(pubobj.ToString()))
                            {
                                completed = true;
                            }
						}
						catch (Exception ex)
						{
                            LogMessage.LogFailure(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_EventManager_Fail_1"), ex, this);

                            try
                            {
                                Publish(ServiceHostState.Shutdown);
                                Environment.Exit(-5);
                            }
                            catch
                            {
                                Environment.Exit(-3);
                            }

                            throw new Exception(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_EventManager_Exception_1") + ex.Message, ex);
						}
			       	};
		}
	}
}