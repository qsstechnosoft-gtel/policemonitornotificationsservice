using System.Configuration;
using System.Xml;
using System.ServiceProcess;
using System.Diagnostics;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
	public class InstallerConfigurationHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			return new InstallerConfiguration(section);
		}
	}

	public class InstallerConfiguration
	{
        public readonly string ServiceName;
        public readonly string ServiceDescription = "";

        public InstallerConfiguration(XmlNode section)
        {
            XmlElement node = section["ServiceName"];
            if (node == null)
            {
                throw new ConfigurationErrorsException(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_InstallerConfiguration_Exception_1"), section);
            }
            ServiceName = node.InnerText;
            InstallerConfigurationCalss.Instance.ServiceName = ServiceName;
            if (string.IsNullOrEmpty(ServiceName))
            {
                throw new ConfigurationErrorsException(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_InstallerConfiguration_Exception_2"), section);
            }

            node = section["ServiceDescription"];
            if (node != null)
            {
                ServiceDescription = node.InnerText;
                InstallerConfigurationCalss.Instance.ServiceDesc = ServiceDescription;
          
            }

        }
	}

    
    public sealed class InstallerConfigurationCalss
    {
        static int _counter = 0;
        static InstallerConfigurationCalss _instance = new InstallerConfigurationCalss();
        static object lockObj = new object();

        private string _serviceName;
        public string ServiceName
        {
            get { return this._serviceName; }
            set
            {
                if (_counter == 1)
                {
                    _counter++;
                    this._serviceName = value;
                }
            }
        }

        private string _serviceDesc;
        public string ServiceDesc
        {
            get { return this._serviceDesc; }
            set
            {
                this._serviceDesc = value;
            }
        }


        private InstallerConfigurationCalss()
        {
            _counter++;
        }

        public static InstallerConfigurationCalss Instance
        {
            get
            {
                lock (lockObj)
                {
                    if (_instance == null)
                    {
                        _counter++;
                        _instance = new InstallerConfigurationCalss();
                    }
                    return _instance;
                }
            }

        }
    }

    public sealed class UseProfileFileMode
    {
        static int count = 0;
        static UseProfileFileMode _instance = new UseProfileFileMode();
        static object lockObj = new object();

        private bool _getAndPostLastV1CheckDateInMantis = false;
        public bool GetAndPostLastV1CheckDateInMantis
        {
            get { return this._getAndPostLastV1CheckDateInMantis; }
            set
            {
                if (count == 1)
                {
                    count++;
                    this._getAndPostLastV1CheckDateInMantis = value;
                }
            }
        }

        private UseProfileFileMode()
        {
            count++;
        }

        public static UseProfileFileMode Instance
        {
            get
            {
                lock (lockObj)
                {
                    if (_instance == null)
                    {
                        count++;
                        _instance = new UseProfileFileMode();
                    }
                    return _instance;
                }
            }

        }
    }
    public static class ServiceProgram
    {


        public static void StopWindowServiceFromCMD(string servicename)
        {
            Process process = new Process();
            process.StartInfo.FileName = "cmd";
            process.StartInfo.Arguments = "/c net stop \"" + servicename +"\"";
            process.Start();
        }

        public static void StartWindowServiceFromCMD(string servicename)
        {
            ServiceController ctrl = new ServiceController(servicename);
            if (ctrl.Status == ServiceControllerStatus.Stopped)
            {
                Process process = new Process();
                process.StartInfo.FileName = "cmd";
                process.StartInfo.Arguments = "/c net start \"" + servicename + "\"";
                process.Start();
            }
           
        }
    }

    
}