/*(c) Copyright 2009, VersionOne, Inc. All rights reserved. (c)*/
using System;
using System.Collections.Generic;
using System.Threading;
using PoliceMonitorNotifications.ServiceHost.Resources;
using PoliceMonitorNotifications.ServiceHost.Logging;
using PoliceMonitorNotifications.Profile;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.Core;

namespace PoliceMonitorNotifications.ServiceHost
{
    public class FirstSyncV1 
    {
        private IList<string> _defectList;
        public IList<string> DefectList
        {
            set
            {
                _defectList=value;
            }
            get
            {
                return _defectList;
            }
        }

        private long _lastV1CheckTimeTicks;
        public long LastV1CheckTimeTicks
        {
            set
            {
                _lastV1CheckTimeTicks = value;
            }
            get
            {
                return _lastV1CheckTimeTicks;
            }
        }
    }
    public class FirstSyncMantis
    {
        private IList<string> _issueList;
        public IList<string> IssueList
        {
            set
            {
                _issueList = value;
            }
            get
            {
                return _issueList;
            }
        }
    }

    
	public abstract class CommonMode
	{
		private IEventManager _eventmanager;
		protected IEventManager EventManager
		{
			get
			{
				if (_eventmanager == null)
					_eventmanager = new EventManager();
				return _eventmanager;
			}
		}

		private IList<ServiceInfo> _services;
		protected IList<ServiceInfo> Services
		{
			get
			{
				if (_services == null)
					_services = (IList<ServiceInfo>)System.Configuration.ConfigurationManager.GetSection("Services");		
				return _services;
			}
		}

                private IList<ProjectsMap> _projectsMap;
                public IList<ProjectsMap> ProjectsMap
                {
                        get
                        {
                                if (_projectsMap == null)
                                {
                                        _projectsMap = (IList<ProjectsMap>)System.Configuration.ConfigurationManager.GetSection("ProjectsMap");
                                }
                                return _projectsMap;
                        }
                }

                private ConnectorConfiguration _connector;
                public ConnectorConfiguration ConnectorConfiguration
                {
                    get
                    {
                        if (_connector == null)
                        {
                            _connector = (ConnectorConfiguration)System.Configuration.ConfigurationManager.GetSection("Connector");
                        }
                        return _connector;
                    }
                }


		private IProfileStore _profilestore;
		public IProfileStore ProfileStore
		{
			get
			{
				if (_profilestore == null)
					_profilestore = new XmlProfileStore("profile.xml");
				return _profilestore;
			}
		}

 
		protected void Startup(bool testMode)
		{
			AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            _projectsMap = ProjectsMap;
            ConnectorConfiguration connector = ConnectorConfiguration;

			foreach (ServiceInfo ss in Services)
			{
                LogMessage.Log(LogMessage.SeverityType.Debug, string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_CommonMode_Debug_1"), ss.Name), EventManager);
                ss.Service.Initialize(ss.Config, EventManager, _projectsMap, testMode);
                LogMessage.Log(LogMessage.SeverityType.Debug, string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_CommonMode_Debug_2"), ss.Name), EventManager);
    		}

           
            LogMessage.Log(LogMessage.SeverityType.Info, ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_CommonMode_Info_2"), EventManager);
            LogMessage.Log(LogMessage.SeverityType.Info, ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_CommonMode_Info_1"), EventManager);
            LogMessage.Log(LogMessage.SeverityType.Info, ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_CommonMode_Info_5"), EventManager);
            
            EventManager.Publish(ServiceHostState.Startup);
		}

		private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
            LogMessage.Log(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_CommonMode_Exception_1"), (Exception)e.ExceptionObject, EventManager); 
		}

		protected void Shutdown()
		{
			EventManager.Publish(ServiceHostState.Shutdown);
			Thread.Sleep(5 * 1000);
            ProfileStore.Flush();
		}

	}
}