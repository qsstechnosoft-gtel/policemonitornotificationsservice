﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliceMonitorNotifications.ServiceHost.Resources
{
    public static class ResourceManager
    {
        public static string GetResourceString(string key)
        {
            return Resources.Properties.Resources.ResourceManager.GetString(key).Replace(@"\t", "\t").Replace(@"\n", "\n").Replace(@"\r", "\r");
        }
    }
}
