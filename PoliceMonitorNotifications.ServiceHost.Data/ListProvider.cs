﻿using System;
using System.Collections.Generic;
//using System.Data.Entity.Core.Objects;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LETG.Data
{
    public static class ListProvider
    {
        static LETGEntities db = null;

        static ListProvider()
        {
            db = new LETGEntities();
        }


        public static List<System.Net.Mail.MailAddress> GetEmailIDs(int RoleID)
        {
            using (var TDB = new LETGEntities())
            {
                return TDB.Users.Where(uSR => uSR.Active == true && uSR.RoleID == RoleID)
                    .Select(uSR => new { EmailID = uSR.EmailAddress, DisplayName = uSR.FirstName + " " + uSR.LastName })
                    .ToList()
                    .Select(uSR => new System.Net.Mail.MailAddress(uSR.EmailID, uSR.DisplayName)).ToList();
            }
        }

        public static List<Role> GetRoles(bool Active = true)
        {
            using (var TDB = new LETGEntities())
            {
                return TDB.Roles.Where(rL => rL.Active == Active).OrderBy(rL => rL.Name).ToList();
            }
        }

        public static List<Agency> GetAgencies(bool Active = true)
        {
            using (var TDB = new LETGEntities())
            {
                return TDB.Agencies.Where(rL => rL.Active == Active).OrderBy(rL => rL.Name).ToList();
            }
        }

        public static List<Title> GetTitles(bool Active = true)
        {
            using (var TDB = new LETGEntities())
            {
                return TDB.Titles.Where(rL => rL.Active == Active).OrderBy(rL => rL.Name).ToList();
            }
        }

        public static List<City> GetCities(bool Active = true)
        {
            using (var TDB = new LETGEntities())
            {
                return TDB.Cities.Where(sT => sT.Active == Active).OrderBy(sT => sT.Name).ToList();
            }
        }

        public static List<NotificationType> GetNotifications(bool Active = true)
        {
            using (var TDB = new LETGEntities())
            {
                return TDB.NotificationTypes.Where(nT => nT.Active == Active).OrderBy(nT => nT.Type).ToList();
            }
        }

        public static List<T> GetUser<T>(LETG.Entities.Grid.DataHandler<T> model, Func<string, string> GetOrderByFunction)
        {
            model.OrderBy = GetOrderByFunction(model.OrderBy);
            var _totalRecords = new ObjectParameter("TotalRecords", typeof(int));
            var _users = db.GetUsers(model.StartDate, model.EndDate, model.PageSize, model.PageNumber, model.OrderBy, model.OrderByAscending ? "ASC" : "DESC", model.SearchQuery, _totalRecords).Select<Data.GetUsers_Result, T>(rQ => (T)Convert.ChangeType(rQ, typeof(T))).ToList();
            model.TotalRecords = Convert.ToInt32(_totalRecords.Value);
            return _users;
        }

        public static List<T> GetAgencies<T>(LETG.Entities.Grid.DataHandler<T> model, Func<string, string> GetOrderByFunction)
        {
            model.OrderBy = GetOrderByFunction(model.OrderBy);
            var _totalRecords = new ObjectParameter("TotalRecords", typeof(int));
            var _agencies = db.GetAgencies(model.StartDate, model.EndDate, model.PageSize, model.PageNumber, model.OrderBy, model.OrderByAscending ? "ASC" : "DESC", model.SearchQuery, _totalRecords).Select<Data.GetAgencies_Result, T>(rQ => (T)Convert.ChangeType(rQ, typeof(T))).ToList();
            model.TotalRecords = Convert.ToInt32(_totalRecords.Value);
            return _agencies;
        }

        public static List<T> GetNotifications<T>(LETG.Entities.Grid.DataHandler<T> model, Func<string, string> GetOrderByFunction)
        {
            model.OrderBy = GetOrderByFunction(model.OrderBy);
            var _totalRecords = new ObjectParameter("TotalRecords", typeof(int));
            var _notifications = db.GetNotifications(model.StartDate, model.EndDate, model.PageSize, model.PageNumber, model.OrderBy, model.OrderByAscending ? "ASC" : "DESC", model.SearchQuery, _totalRecords).Select<Data.GetNotifications_Result, T>(rQ => (T)Convert.ChangeType(rQ, typeof(T))).ToList();
            model.TotalRecords = Convert.ToInt32(_totalRecords.Value);
            return _notifications;
        }
    }
}

