﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using EntityFramework.BulkInsert.Extensions;
using System.Data.Entity.Core.Objects;
//using System.Runtime.Caching;


namespace PoliceMonitorNotifications.ServiceHost.Data
{
    public class CustomerNotification
    {
        public bool Active { get; set; }
        public int? AfterFirstTransmissionWaitInterval { get; set; }
        public int? AfterNSecTransmissionWaitInterval { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }

        public int CustomerNotificationId { get; set; }

        public int? Interval { get; set; }

        public int? LastNSecTransmissionsCount { get; set; }

        public string NotificationName { get; set; }

        public int? NotifiyTransmissionsCount { get; set; }

        public int? NSecInterval { get; set; }
        public List<GroupModel> Groups { get; set; }
        public CustomerNotificationSettingModel CustomerNotificationSetting { get; set; }
    }
    public class UserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }

        public List<int> TalkGroups { get; set; }
    }




    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        //public DateTime LastScanUniversalDateTime { get; set; }

        public CustomerNotificationSettingModel CustomerNotificationSetting { get; set; }
        //public List<UserModel> Users { get; set; }
        public List<long> TalkGroups { get; set; }
        public List<GroupModel> Groups { get; set; }

    }
    public class NotificationSettingRadio
    {
        public int CustomerId { get; set; }
        public int CustomerNotificationId { get; set; }
        public int NotificationSettingRadioMapId { get; set; }
        public int RadioId { get; set; }
        
    }
    public class CustomerNotificationSettingModel
    {
        //public DateTime LastScanUniversalDateTime { get; set; }
        public int CustomerNotificationId { get; set; }
        // CustomerNotificationId = s.CustomerNotificationId,
        public int CustomerId { get; set; }
        public string NotificationName { get; set; }
        public int? NotifiyTransmissionsCount { get; set; }
        public int? Interval { get; set; }

        //Case 2
        public int? AfterFirstTransmissionWaitInterval { get; set; }


        //Case 3
        public int? LastNSecTransmissionsCount { get; set; }
        public int? LastNSecInterval { get; set; }
        public int? AfterNSecTransmissionWaitInterval { get; set; }
    }

    public class CustomerSettingCase
    {
        public int CustomerId { get; set; }
        public long TalkGroupId { get; set; }
        public DateTime NextScanUniversalDateTime { get; set; }
        public int SystemID { get; set; }
        public string CustomerSettingCaseName { get; set; }
    }

    public class GroupModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public List<int> TalkGroups { get; set; }
        public List<UserModel> Users { get; set; }
    }
    public class SqlServerDao
    {

        //public static string GetAllCustomersData = "GetAllCustomersData";
        //static ObjectCache cache = MemoryCache.Default;

        public static Dictionary<Int32, List<TalkGroup>> GetAllDistinctTalkGroups()
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var query = from cust in db.Customers
                                join custtalkgrp in db.CustomerTalkGroups on cust.CustomerId equals custtalkgrp.CustomerId
                                where cust.Active == true
                                select (new { custtalkgrp.TalkGroup, custtalkgrp.SystemID });

                    Dictionary<int, List<TalkGroup>> lst = new Dictionary<int, List<TalkGroup>>();

                    // systemid, talkgroup id int array

                    var keys = query.Select(s => s.SystemID).Distinct().ToList();
                    foreach (var systemId in keys)
                    {
                        int sysId = (systemId == null) ? 1039 : systemId.Value;

                        //List<int> talkgroupId = new List<int>();
                        List<string> results = query.Where(w => w.SystemID == systemId.Value).Select(s => s.TalkGroup).Distinct().ToList();
                        List<int> talkgroupIds = Array.ConvertAll(results.ToArray(), new Converter<string, int>(s => { return int.Parse(s); })).ToList();
                        List<TalkGroup> talkgroups = db.TalkGroups.Where(x => talkgroupIds.Contains(x.ID) && x.SystemId == sysId).ToList();
                        
                        lst.Add(sysId, talkgroups);
                    }
                    return lst;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Radio> GetAllRadios()
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    return db.Radios.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task SaveNotificationLog(int customerId, int userId, int talkGroupId, int radioId, string notificationMsg, byte[] mp3File)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {

                    db.NotificationLogs.Add(new NotificationLog()
                    {
                        CustomerId = customerId,
                        UserId = userId,
                        TalkGroupId = talkGroupId,
                        NotificationMsg = notificationMsg,
                        NotificationDate = DateTime.Now,
                        MP3File = mp3File
                    });
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static async Task SaveTransmissions(string talkGroupItemId, long transStartMSec, long transLengthMSec, long radioID, long talkGroupID, DateTime transStartUTC,
                                                   string transTimeDesc, string radioDesc, string channelName, int systemID)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    //                    if (!db.PolledTalkGroupItems.Where(x => x.TalkGroupItemId == talkGroupItemId).Any())
                    {
                        db.PolledTalkGroupItems.Add(new PolledTalkGroupItem()
                        {

                            TalkGroupItemId = talkGroupItemId,
                            TransStartMSec = transStartMSec,
                            TransLengthMSec = transLengthMSec,
                            RadioID = radioID,
                            TalkGroupID = talkGroupID,
                            TransStartUTC = transStartUTC,
                            TransTimeDesc = transTimeDesc,
                            RadioDesc = radioDesc,
                            ChannelName = channelName,
                            SystemID = systemID
                        });
                        await db.SaveChangesAsync();
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// This function will return all the notifications setup for customer
        /// </summary>
        /// <returns></returns>
        public static async Task<List<CustomerModel>> GetCustomerConfiguredNotifications()
        {
            try
            {
                //if (!cache.Contains(GetAllCustomersData))
                {

                    using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                    {

                        var customer = db.Customers.ToList();

                        var query = db.CustomerNotificationSettingNews.Where(w => w.Active == true).Select(cust => new CustomerModel()
                                    {
                                        CustomerId = cust.CustomerId,
                                        CustomerName = db.Customers.Where(w => w.CustomerId == cust.CustomerId).Select(s => s.CustomerName).FirstOrDefault(),
                                        //TalkGroups = db.NotificationSettingTalkGroups.Where(x => x.CustomerNotificationId == cust.CustomerNotificationId).Select(x => x.TakGroupId).AsEnumerable().Cast<long>().ToList(),
                                        Groups = db.Groups.Where(x => x.CustomerId == cust.CustomerId).Select(grp => new GroupModel()
                                        {
                                            GroupId = grp.GroupId,
                                            GroupName = grp.GroupName,
                                            TalkGroups = db.NotificationSettingTalkGroups.Where(x=>x.CustomerNotificationId==cust.CustomerNotificationId).Select(x => x.TakGroupId).ToList(),
                                            Users = db.UserGroups.Where(x => x.GroupId == grp.GroupId && x.CustomerId == cust.CustomerId && x.User.Active == true).Select(usr => new UserModel()
                                            {
                                                UserId = usr.UserId,
                                                UserName = usr.User.UserName,
                                                Email = usr.User.Email,
                                                Address = usr.User.Address,
                                                City = usr.User.City,
                                                State = usr.User.State,
                                                PostalCode = usr.User.PostalCode,
                                                PhoneNumber = usr.User.PhoneNumber
                                            }).ToList()
                                        }).ToList(),
                                        CustomerNotificationSetting = db.CustomerNotificationSettingNews.Where(x => x.CustomerId == cust.CustomerId && x.CustomerNotificationId==cust.CustomerNotificationId && x.Active==true).Select(sett => new CustomerNotificationSettingModel()
                                        {
                                            CustomerId = sett.CustomerId,
                                            NotificationName=sett.NotificationName,
                                            NotifiyTransmissionsCount = sett.NotifiyTransmissionsCount,
                                            Interval = sett.Interval,
                                            AfterFirstTransmissionWaitInterval = sett.AfterFirstTransmissionWaitInterval,
                                            LastNSecInterval = sett.NSecInterval,
                                            LastNSecTransmissionsCount = sett.LastNSecTransmissionsCount,
                                            AfterNSecTransmissionWaitInterval = sett.AfterNSecTransmissionWaitInterval,
                                            CustomerNotificationId=sett.CustomerNotificationId
                                        }).FirstOrDefault()


                                    });
                        
                        //cache.Add(GetAllCustomersData, query.ToListAsync(), GetCacheItemPolicy(1));
                        return await query.ToListAsync();
                    }
                }
                //return (List<CustomerModel>) cache.Get(GetAllCustomersData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //private static CacheItemPolicy GetCacheItemPolicy(int hours)
        //{
        //    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
        //    cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(hours);
        //    return cacheItemPolicy;
        //}

        public static async Task<List<PolledTalkGroupItem>> GetCustomerTransmissions(List<long> talkGroups, DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var query = from talkGroupItem in db.PolledTalkGroupItems
                                where talkGroups.Contains(talkGroupItem.TalkGroupID.Value) && talkGroupItem.TransStartUTC >= fromDate && talkGroupItem.TransStartUTC <= toDate
                                select talkGroupItem;

                    return await query.Distinct().ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static async Task<List<PolledTalkGroupItem>> GetCustomerTransmissionsForTalkGroup(long talkGroup, DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var query = from talkGroupItem in db.PolledTalkGroupItems
                                where talkGroupItem.TalkGroupID == talkGroup && talkGroupItem.TransStartUTC >= fromDate && talkGroupItem.TransStartUTC <= toDate
                                select talkGroupItem;

                    return await query.Distinct().ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static async Task<DateTime> GetLastScannedTime(dynamic talk, int NotificationID)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    int talkid = Convert.ToInt32(talk[1]);
                    int sysid = Convert.ToInt32(talk[0]);
                    //var lastScanDate = await db.NotificationSettingTalkGroups.Where(x => x.CustomerNotificationId == NotificationID && x.SystemID==sysid&&x.TakGroupId==talkid).Select(x  => new{ x.LastScanUniversalDateTime,x.NotificationSettingTalkGroupId}).FirstOrDefaultAsync();
                    var lastScanDate = await db.NotificationSettingTalkGroups.Where(x => x.CustomerNotificationId == NotificationID && x.SystemID == sysid && x.TakGroupId == talkid).FirstOrDefaultAsync();

                    if (lastScanDate.LastScanUniversalDateTime == null)
                    {
                        var isExist = db.NotificationSettingTalkGroups.Where(w=>w.NotificationSettingTalkGroupId==lastScanDate.NotificationSettingTalkGroupId).FirstOrDefault();
                        //db.NotificationSettingTalkGroups.Add(new NotificationSettingTalkGroup()
                        //{
                        //   NotificationSettingTalkGroupId=lastScanDate.NotificationSettingTalkGroupId,
                        //   CustomerId=lastScanDate.CustomerId,
                        //   CustomerNotificationId=lastScanDate.CustomerNotificationId,
                           
                        //    LastScanUniversalDateTime = DateTime.Now.ToUniversalTime()
                        //});
                       // await db.SaveChangesAsync();
                        isExist.LastScanUniversalDateTime = DateTime.Now.ToUniversalTime();
                        db.SaveChanges();
                        return DateTime.Now.ToUniversalTime();
                    }
                    else
                    {
                        return Convert.ToDateTime( lastScanDate.LastScanUniversalDateTime);
                    } 

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }




        public static async Task<DateTime> GetLastScannedTimestamp(int customerId, string NotificationName)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var lastScanDate = await db.CustomerNotificationLastScans.Where(x => x.CustomerId == customerId && x.NotificationName.Trim()==NotificationName.Trim()).Select(x => x.LastScanUniversalDateTime).FirstOrDefaultAsync();

                    if (lastScanDate == null)
                    {
                        db.CustomerNotificationLastScans.Add(new CustomerNotificationLastScan()
                        {
                            CustomerId = customerId,
                            LastScanUniversalDateTime = DateTime.Now.ToUniversalTime(),
                            NotificationName=NotificationName
                        });
                        await db.SaveChangesAsync();

                        return DateTime.Now.ToUniversalTime();
                    }
                    else
                    {
                        return lastScanDate.Value;
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }






        public static async Task SetLastScannedTimestampUTC(dynamic talk, int NotificationID,DateTime date)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    int talkid = Convert.ToInt32(talk[1]);
                    int sysid = Convert.ToInt32(talk[0]);
                    var lastScanDate = await db.NotificationSettingTalkGroups.Where(x => x.CustomerNotificationId == NotificationID && x.SystemID == sysid && x.TakGroupId == talkid).FirstOrDefaultAsync();

                    if (lastScanDate != null)
                    {
                        var isExist = db.NotificationSettingTalkGroups.Where(w => w.NotificationSettingTalkGroupId == lastScanDate.NotificationSettingTalkGroupId).FirstOrDefault();
                        isExist.LastScanUniversalDateTime = date;//DateTime.Now.ToUniversalTime();
                       await db.SaveChangesAsync();
                       // return DateTime.Now.ToUniversalTime();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        public static async Task SetLastScannedTimestampUTC(int customerId, DateTime nextScan, string NotificationName)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var customerNotificationLastScan = await db.CustomerNotificationLastScans.Where(x => x.CustomerId == customerId && x.NotificationName.Trim() == NotificationName.Trim()).FirstOrDefaultAsync();

                    if (customerNotificationLastScan == null)
                    {
                        db.CustomerNotificationLastScans.Add(new CustomerNotificationLastScan()
                        {
                            CustomerId = customerId,
                            LastScanUniversalDateTime = nextScan,
                            NotificationName = NotificationName
                            
                        });
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        customerNotificationLastScan.LastScanUniversalDateTime = nextScan;
                        await db.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public static async Task Archive_PolledTalkGroupItem(DateTime scanDateTime)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {

                    var oldTalkGroupItems = await db.PolledTalkGroupItems.Where(x => x.TransStartUTC.Value <= scanDateTime).ToListAsync();

                    if (oldTalkGroupItems != null)
                    {
                        bool flag = false;
                        foreach (var oldTalkGroupItem in oldTalkGroupItems)
                        {
                            db.Archive_PolledTalkGroupItem.Add(new Archive_PolledTalkGroupItem()
                            {
                                ID = oldTalkGroupItem.ID,
                                TalkGroupItemId = oldTalkGroupItem.TalkGroupItemId,
                                TransStartMSec = oldTalkGroupItem.TransStartMSec,
                                TransLengthMSec = oldTalkGroupItem.TransLengthMSec,
                                RadioID = oldTalkGroupItem.RadioID,
                                TalkGroupID = oldTalkGroupItem.TalkGroupID,
                                TransStartUTC = oldTalkGroupItem.TransStartUTC,
                                TransTimeDesc = oldTalkGroupItem.TransTimeDesc,
                                RadioDesc = oldTalkGroupItem.RadioDesc,
                                ChannelName = oldTalkGroupItem.ChannelName
                            });
                            db.PolledTalkGroupItems.Remove(oldTalkGroupItem);
                            flag = true;
                        }
                        if (flag == true)
                        {
                            await db.SaveChangesAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public class MyContext : DbContext
        {
            public DbSet<PolledTalkGroupItem> PolledTalkGroupItems { get; set; }
        }

        public static async Task SaveTransmissions(List<PolledTalkGroupItem> FilteredList)
        {
            try
            {
                //await SqlServerDao.SaveTransmissions(talkGroupItem.ID, talkGroupItem.TransStartMSec, talkGroupItem.TransLengthMSec, talkGroupItem.RadioID,
                //    talkGroupItem.TalkGroupID, talkGroupItem.TransStartUTC, talkGroupItem.TransTimeDesc, talkGroupItem.RadioDesc, (string)tkgroups["" + talkGroupItem.TalkGroupID]);

                //Console.WriteLine("Total Records: " + FilteredList.Count);
                //Console.WriteLine("Start time: " + DateTime.Now.ToShortTimeString());
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    db.BulkInsert(FilteredList);
                    db.SaveChanges();
                }
                //Console.WriteLine("End time: " + DateTime.Now.ToShortTimeString());
                //using (var context = new DbContext(""))
                //{
                //    context.BulkInsert(FilteredList);
                //    context.SaveChanges();
                //}
                //using (var ctx = GetDbContextFromEntity(FilteredList))
                //{
                //    using (var transactionScope = new TransactionScope())
                //    {
                //        // some stuff in dbcontext

                //        ctx.BulkInsert(FilteredList);

                //        ctx.SaveChanges();
                //        transactionScope.Complete();
                //    }
                //}

                //using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                //{
                //    db.Configuration.AutoDetectChangesEnabled = false;
                //    db.PolledTalkGroupItems.AddRange(FilteredList);
                //    await db.SaveChangesAsync();
                //}
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                throw;
            }
        }

        public static List<TblSystem> GetSystemsList()
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                return db.TblSystems.ToList();
            }
        }

        public static async Task Savesystem(List<TblSystem> FilteredList)
        {
            if (FilteredList.Count <= 0)
                return;
            try
            {
                Console.WriteLine("Total Records: " + FilteredList.Count);
                Console.WriteLine("Start time: " + DateTime.Now.ToShortTimeString());
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    db.Database.ExecuteSqlCommand("TRUNCATE TABLE [dbo].[TblSystems]");
                    db.BulkInsert(FilteredList);
                    db.SaveChanges();
                }
                Console.WriteLine("End time: " + DateTime.Now.ToShortTimeString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void SaveRadios(List<Radio> FilteredList, bool flag)
        {
            if (FilteredList.Count <= 0)
                return;
            try
            {
                Console.WriteLine("Total Records: " + FilteredList.Count);
                Console.WriteLine("Start time: " + DateTime.Now.ToShortTimeString());
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    if (flag)
                    {
                        db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.[Radios]");
                    }
                    db.BulkInsert(FilteredList);
                    db.SaveChanges();
                }
                Console.WriteLine("End time: " + DateTime.Now.ToShortTimeString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void SaveTalkGroups(List<TalkGroup> FilteredList, bool flag)
        {
            if (FilteredList.Count <= 0)
                return;
            try
            {
                Console.WriteLine("Total Records: " + FilteredList.Count);
                Console.WriteLine("Start time: " + DateTime.Now.ToShortTimeString());
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    if (flag)
                    {
                        db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.[TalkGroups]");
                    }

                    db.BulkInsert(FilteredList);
                    db.SaveChanges();
                }
                Console.WriteLine("End time: " + DateTime.Now.ToShortTimeString());
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
