using System;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading;
using System.Collections;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
	internal class ServiceUtil
	{
        #region DLLImport
        private const int SC_MANAGER_CREATE_SERVICE = 0x0002;
        private const int SERVICE_WIN32_OWN_PROCESS = 0x00000010;
        private const int SERVICE_ERROR_NORMAL = 0x00000001;
        private const int STANDARD_RIGHTS_REQUIRED = 0xF0000;
        private const int SERVICE_QUERY_CONFIG = 0x0001;
        private const int SERVICE_CHANGE_CONFIG = 0x0002;
        private const int SERVICE_QUERY_STATUS = 0x0004;
        private const int SERVICE_ENUMERATE_DEPENDENTS = 0x0008;
        private const int SERVICE_START = 0x0010;
        private const int SERVICE_STOP = 0x0020;
        private const int SERVICE_PAUSE_CONTINUE = 0x0040;
        private const int SERVICE_INTERROGATE = 0x0080;
        private const int SERVICE_USER_DEFINED_CONTROL = 0x0100;
        private const int SERVICE_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED |
                                  SERVICE_QUERY_CONFIG |
                                  SERVICE_CHANGE_CONFIG |
                                  SERVICE_QUERY_STATUS |
                                  SERVICE_ENUMERATE_DEPENDENTS |
                                  SERVICE_START |
                                  SERVICE_STOP |
                                  SERVICE_PAUSE_CONTINUE |
                                  SERVICE_INTERROGATE |
                                  SERVICE_USER_DEFINED_CONTROL);
        private const int SERVICE_AUTO_START = 0x00000002;

        private const int GENERIC_WRITE = 0x40000000;
        private const int DELETE = 0x10000;

        private const int SERVICE_CONFIG_DESCRIPTION = 0x1;
        private const int SERVICE_CONFIG_FAILURE_ACTIONS = 0x2;

        private const int ERROR_ACCESS_DENIED = 5;
        private const int ERROR_INVALID_HANDLE = 6;
        private const int ERROR_SERVICE_MARKED_FOR_DELETE = 1072;
            
        [DllImport("advapi32.dll")]
        private static extern IntPtr OpenSCManager(string lpMachineName, string lpSCDB, int scParameter);

        [DllImport("Advapi32.dll")]
        private static extern IntPtr CreateService(IntPtr SC_HANDLE, string lpSvcName, string lpDisplayName, int dwDesiredAccess, int dwServiceType, int dwStartType, int dwErrorControl, string lpPathName, string lpLoadOrderGroup, int lpdwTagId, string lpDependencies, string lpServiceStartName, string lpPassword);

        [DllImport("advapi32.dll")]
        private static extern void CloseServiceHandle(IntPtr SCHANDLE);

        [DllImport("advapi32.dll")]
        private static extern int StartService(IntPtr SVHANDLE, int dwNumServiceArgs, string lpServiceArgVectors);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern IntPtr OpenService(IntPtr SCHANDLE, string lpSvcName, int dwDesiredAccess);

        [DllImport("advapi32.dll")]
        private static extern int DeleteService(IntPtr SVHANDLE);

        // Win32 function to change the Service Description.
        [DllImport("advapi32.dll", EntryPoint = "ChangeServiceConfig2")]
        public static extern bool ChangeServiceDescription(IntPtr hService, int dwInfoLevel,
         [MarshalAs(UnmanagedType.Struct)] 
         ref SERVICE_DESCRIPTION lpInfo);

        // Win32 function to change the Service Config for the Failure Actions.
        [DllImport("advapi32.dll", EntryPoint = "ChangeServiceConfig2")]
        public static extern bool ChangeServiceFailureActions(IntPtr hService, int dwInfoLevel,
         [MarshalAs(UnmanagedType.Struct)] 
         ref SERVICE_FAILURE_ACTIONS lpInfo);

        [DllImport("kernel32.dll")]
        private static extern int GetLastError();

        #endregion DLLImport

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct SERVICE_DESCRIPTION
        {
            [MarshalAs(UnmanagedType.LPStr)] // use UnmanagedType.LPStr otherwise only the first character will be "seen" !
            public string lpDescription;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct SERVICE_FAILURE_ACTIONS
        {
            public int dwResetPeriod;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpRebootMsg;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpCommand;
            public int cActions;
            public IntPtr lpsaActions;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class SC_ACTION
        {
            public Int32 type;
            public UInt32 dwDelay;
        }

        // Set to Null is equivalent as set with LocalSystem account
        public const string LocalService = null; //"LocalSystem";// "NT AUTHORITY\\LocalService"; ?
        public const string NetworkService = "NT AUTHORITY\\NetworkService";
        public const string LocalSystem = null;

        public static void InstallService(string svcPath, string svcName, string svcDescription, string svcUsername, string svcPassword)
        {
            Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_1"), svcName));
            IntPtr scm = OpenSCManager(null, null, SC_MANAGER_CREATE_SERVICE);
            if (scm.ToInt32() == 0)
            {
                Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_2"));
                return;
            }
            IntPtr svc = OpenService(scm, svcName, SC_MANAGER_CREATE_SERVICE);
            if (svc.ToInt32() != 0)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_3"), svcName));
                return; // Service is Installed
            }

            svc = CreateService(scm, svcName, svcName, SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, svcPath, null, 0, null, svcUsername, svcPassword);
            if (svc.ToInt32() == 0)
            {
                string error = String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_4"), svcName);
                Console.WriteLine(error);
                throw new Exception(error);
            }

            try
            {
                SetServiceDescription(/*svcName*/ svc, svcDescription);
            }
            catch { } // Don't really matter if crash

            SetServiceFailureActions(svc);

            CloseServiceHandle(scm);

            Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_5"), svcName));
        }

        private static void SetServiceDescription(IntPtr svc, string description)
        {
            if (String.IsNullOrEmpty(description))
            {
                return;
            }

            // Set the SERVICE_DESCRIPTION struct
            SERVICE_DESCRIPTION descrStruct = new SERVICE_DESCRIPTION();
            descrStruct.lpDescription = description;

            // Call the ChangeServiceDescription() abstraction of ChangeServiceConfig2()
            bool success = ChangeServiceDescription(svc, SERVICE_CONFIG_DESCRIPTION, /*ref descrStruct*/ ref descrStruct);

            //Check the return
            if (!success)
            {
                int err = GetLastError();
                if (err == ERROR_ACCESS_DENIED)
                {
                    throw new Exception(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_1"));
                }
                else
                {
                    throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_2"), err));
                }
            }
        }


        private static void SetServiceFailureActions(IntPtr svc)
        {
            IntPtr tmpBuf = IntPtr.Zero;
            IntPtr svcLock = IntPtr.Zero;
            ArrayList FailureActions = new ArrayList();

            // First Failure Actions and Delay (msec)
            FailureActions.Add(new FailureAction(RecoverAction.None, 0));
            // Second Failure Actions and Delay (msec)
            FailureActions.Add(new FailureAction(RecoverAction.None, 0));
            // Subsequent Failures Actions and Delay (msec)
            FailureActions.Add(new FailureAction(RecoverAction.None, 0));


            int numActions = FailureActions.Count;
            int[] myActions = new int[numActions * 2];
            int currInd = 0;
            foreach (FailureAction fa in FailureActions)
            {
                myActions[currInd] = (int)fa.Type;
                myActions[++currInd] = fa.Delay;
                currInd++;
            }

            // Need to pack 8 bytes per struct
            tmpBuf = Marshal.AllocHGlobal(numActions * 8);

            // Move array into marshallable pointer
            Marshal.Copy(myActions, 0, tmpBuf, numActions * 2);

            // Set the SERVICE_FAILURE_ACTIONS struct
            SERVICE_FAILURE_ACTIONS sfa = new SERVICE_FAILURE_ACTIONS();
            sfa.cActions = 3;
            sfa.dwResetPeriod = 0;
            sfa.lpCommand = null;
            sfa.lpRebootMsg = null;
            sfa.lpsaActions = new IntPtr(tmpBuf.ToInt32());

            // Call the ChangeServiceFailureActions() abstraction of ChangeServiceConfig2()
            bool success = ChangeServiceFailureActions(svc, SERVICE_CONFIG_FAILURE_ACTIONS, ref sfa);

            // Free the memory
            Marshal.FreeHGlobal(tmpBuf);
            tmpBuf = IntPtr.Zero;

            //Check the return
            if (!success)
            {
                int err = GetLastError();
                if (err == ERROR_ACCESS_DENIED)
                {
                    throw new Exception(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_3"));
                }
                else
                {
                    throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_4"), err));
                }
            }
        }

        public static void StartService(string svcName)
        {
            Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_6"), svcName));
            ServiceController ctrl = new ServiceController(svcName);
            if (ctrl.Status == ServiceControllerStatus.Running)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_7"), svcName));
                return;
            }
            else if (ctrl.Status == ServiceControllerStatus.ContinuePending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_8"), svcName));
            }
            else if (ctrl.Status == ServiceControllerStatus.Paused)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_9"), svcName));
                ctrl.Continue();
            }
            else if (ctrl.Status == ServiceControllerStatus.PausePending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_10"), svcName));
                ctrl.WaitForStatus(ServiceControllerStatus.Paused);
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_11"), svcName));
                ctrl.Continue();
            }
            else if (ctrl.Status == ServiceControllerStatus.StartPending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_12"), svcName));
            }
            else if (ctrl.Status == ServiceControllerStatus.Stopped)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_13"), svcName));
                ctrl.Start();
            }
            else if (ctrl.Status == ServiceControllerStatus.StopPending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_14"), svcName));
                ctrl.WaitForStatus(ServiceControllerStatus.Stopped);
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_15"), svcName));
                ctrl.Start();
            }
            else // Should never be here
            {
                throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_16"), svcName));
            }

            ctrl.WaitForStatus(ServiceControllerStatus.Running);
            Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_17"), svcName));
        }

        public static void StopService(string svcName)
        {
            Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_18"), svcName));
            ServiceController ctrl = new ServiceController(svcName);
            if (ctrl.Status == ServiceControllerStatus.Stopped)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_19"), svcName));
                return;
            }
            else if (ctrl.Status == ServiceControllerStatus.ContinuePending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_20"), svcName));
                //ctrl.WaitForStatus(ServiceControllerStatus.Running);
                //Console.WriteLine(String.Format("Service '{0}' current Status: 'Started'.\nStopping it ...", svcName));
                ctrl.Stop();
            }
            else if (ctrl.Status == ServiceControllerStatus.Paused)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_21"), svcName));
                ctrl.Stop();
            }
            else if (ctrl.Status == ServiceControllerStatus.PausePending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_22"), svcName));
                //ctrl.WaitForStatus(ServiceControllerStatus.Paused);
                //Console.WriteLine(String.Format("Service '{0}' current Status: 'Paused'.\nStopping it ...", svcName));
                ctrl.Stop();
            }
            else if (ctrl.Status == ServiceControllerStatus.StartPending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_23"), svcName));
                //ctrl.WaitForStatus(ServiceControllerStatus.Running);
                //Console.WriteLine(String.Format("Service '{0}' current Status: 'Started'.\nStopping it ...", svcName));
                ctrl.Stop();
            }
            else if (ctrl.Status == ServiceControllerStatus.Running)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_24"), svcName));
                ctrl.Stop();
            }
            else if (ctrl.Status == ServiceControllerStatus.StopPending)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_25"), svcName));
            }
            else // Should never be here
            {
                throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_26"), svcName));
            }

            ctrl.WaitForStatus(ServiceControllerStatus.Stopped);
            Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_27"), svcName));
        }

        public static void UnInstallService(string svcName)
        {
            Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_28"), svcName));
            IntPtr sc_hndl = OpenSCManager(null, null, GENERIC_WRITE);
            if (sc_hndl.ToInt32() != 0)
            {
                IntPtr svc_hndl = OpenService(sc_hndl, svcName, DELETE);
                if (svc_hndl.ToInt32() != 0)
                {
                    Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_29"), svcName));
                    int i = DeleteService(svc_hndl);
                    CloseServiceHandle(sc_hndl);
                    if (i != 0)
                    {
                        Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_30"), svcName));
                    }
                    else // ! there is an Error
                    {
                        //Check the error
                        int err = GetLastError();
                        if (err == ERROR_ACCESS_DENIED)
                        {
                            throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_5"), svcName));
                        }
                        else if (err == ERROR_INVALID_HANDLE)
                        {
                            throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_6"), svcName));
                        }
                        else if (err == ERROR_SERVICE_MARKED_FOR_DELETE)
                        {
                            throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_7"), svcName));
                        }
                        else
                        {
                            throw new Exception(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_8"), svcName, err));
                        }
                    }
                }
                else // svc_hndl.ToInt32() == 0
                {
                    Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Debug_31"), svcName));
                }
            }
            else // sc_hndl.ToInt32() == 0
            {
                throw new Exception(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceUtil_Exception_9"));
            }
        }
    }

    internal class FailureAction
    {
        private RecoverAction type = RecoverAction.None;
        private int delay = 0;

        // Default constructor
        public FailureAction() { }

        // Constructor
        public FailureAction(RecoverAction actionType, int actionDelay)
        {
            this.type = actionType;
            this.delay = actionDelay;
        }

        // Property to set recover action type
        public RecoverAction Type
        {
            get { return type; }
            set
            {
                type = value;
            }
        }

        // Property to set recover action delay
        public int Delay
        {
            get { return delay; }
            set
            {
                delay = value;
            }
        }
    }

    internal enum RecoverAction
    {
        None = 0, Restart = 1, Reboot = 2, RunCommand = 3
    }
}