using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
	public class Program
	{
        [STAThread]
        static void Main(string[] args)
        {
            Environment.CurrentDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            if (args.Length == 0)
            {
                RunConsole();
            }
            else if (args.Length == 1)
            {
                if (args[0] == "-install")
                {
                    InstallService();
                }
                else if (args[0] == "-start")
                {
                    StartService();
                }
                else if (args[0] == "-stop")
                {
                    StopService();
                }
                else if (args[0] == "-uninstall")
                {
                    UninstallService();
                }
                else if (args[0] == "--service")
                {
                    // "--service" argument comes automatically when the Service is Started; user should not use it.
                    if (Environment.UserInteractive)
                    {
                        Help();
                    }
                    else
                    {
                        RunService();
                    }
                }
                else if (args[0] == "-version")
                {
                    ShowVersion();
                }
                else
                {
                    Help();
                }
            }
            else if (args.Length == 2)
            {
                if (args[0] == "-install" && args[1] == "-start")
                {
                    InstallAndStartService();
                }
                else
                {
                    Help();
                }
            }
            else
            {
                Help();
            }
        }

        private static string GetVersion()
        {
            try
            {
                Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
                return assembly.GetName().Version.ToString();
            }
            catch
            {
                return null;
            }
        }

        private static void ShowVersion()
        {
            string version = GetVersion();
            if (! String.IsNullOrEmpty(version))
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Debug_1"), version));
            }
            else
            {
                Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Debug_2"));
            }
        }

        private static void InstallService()
        {
            try
            {
                string version = GetVersion();
                string serviceDescription = "";
                if (String.IsNullOrEmpty(version))
                {
                    serviceDescription = Config.ServiceDescription;
                }
                else
                {
                    serviceDescription = String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Debug_3"), Config.ServiceDescription, version);
                }
                ServiceUtil.InstallService("\"" + Assembly.GetEntryAssembly().Location + "\" --service", Config.ServiceName, serviceDescription, ServiceUtil.LocalService, null);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Exception_1"), Config.ServiceName, e.Message));
            }
        }

        private static void StartService()
        {
            try
            {
                ServiceUtil.StartService(Config.ServiceName);
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Exception_2"), Config.ServiceName, e.Message));
            }
        }

        private static void InstallAndStartService()
        {
            InstallService();
            StartService();
        }

        private static void StopService()
        {
            try
            {
                ServiceUtil.StopService(Config.ServiceName);
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Exception_3"), Config.ServiceName, e.Message));
            }
        }

		private static void UninstallService()
		{
            try
            {
                ServiceUtil.StopService(Config.ServiceName);
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Exception_4"), Config.ServiceName, e.Message));
                return;
            }
            try
            {
                ServiceUtil.UnInstallService(Config.ServiceName);
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Exception_5"), Config.ServiceName, e.Message));
            }
		}

		private static void RunService()
		{
            ServiceBase.Run(new NTService(Config.ServiceName));
		}

		private static void RunConsole()
		{
			new ConsoleMode().Run();
		}        
		private static void Help()
		{
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Help_7"));
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Help_1"));
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Help_2"));
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Help_3"));
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Help_4"));
            //Console.WriteLine("\t\t-service\t\tRuns Windows Service");
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Help_5"));
            Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Help_6"));
		}

        private static InstallerConfiguration _config;
        private static InstallerConfiguration Config
        {
            get
            {
                if (_config == null)
                {
                    string MailFrom = ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Info_1");
                    string MailTo = ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Info_2");
                    string MailServer = ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Info_3");
                    string userName = ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Info_5");
                    string password = ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Info_6");
                    XmlDocument document = new XmlDocument();
                    string XMLFileName = string.Empty;
                    string ConnectorType = string.Empty;
                    try
                    {
                        XMLFileName = Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + ".config";
                        document.Load(XMLFileName);
                        _config = (PoliceMonitorNotifications.ServiceHost.InstallerConfiguration)System.Configuration.ConfigurationManager.GetSection("Installer");
                    }
                    catch (Exception ex)
                    {
                        string MailSubject = ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Info_4");
                        string FileNameExe = string.Empty;
                        FileNameExe = Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                        if (FileNameExe == "V1-Mantis_InitialRun_Connector.exe")
                        {
                            ConnectorType = "IR";
                        }
                        else
                        {
                            ConnectorType = "Normal";
                        }

                        string MailBody = string.Format(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_Program_Info_7"), ex.Message).Replace(@"\t", "\t").Replace(@"\n", "\n").Replace(@"\r", "\r").Replace("\\", @"\\").Replace("'", "\'").Replace("\"", "\\\"");
                        string path = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName + ".config";
                        path = path.Replace(XMLFileName, "");
                        path = "\"" + path + "MailUtility\\SendMail\"";
                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        string strArguments = " " + ConnectorType + " " + userName + " " + password + " " + MailFrom + " \"" + MailTo + "\" " + MailServer + " \"" + MailSubject + "\" \"" + @MailBody + " \"";
                        process.StartInfo.FileName = path;
                        process.StartInfo.Arguments = strArguments;// safatima December82 sfatima@axway.com sfatima@axway.com wptxcas1.ptx.axway.int MailSubject MailBody";
                        process.Start();
                    }
                }
                return _config;
            }
        }
	}
}