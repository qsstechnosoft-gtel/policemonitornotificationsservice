using System.ServiceProcess;
using System.Threading;

namespace PoliceMonitorNotifications.ServiceHost
{
	internal class NTService : ServiceBase
	{
		private ServiceMode _servicemode = new ServiceMode();
        private Thread workerThread;

        public NTService(string serviceName)
		{
            ServiceName = serviceName;
            CanShutdown = true;
		}

		protected override void OnStart(string[] args)
		{
            //RequestAdditionalTime(120000); // requesting additional time
			base.OnStart(args);
            // Starting the real work on a separate thread to allow a quick return at OnStart().
            workerThread = new Thread(new ThreadStart(StartWorkerThread));
            workerThread.Start();
		}

        private void StartWorkerThread()
        {
            _servicemode.Start();
        }

		protected override void OnStop()
		{
			_servicemode.Stop();
			base.OnStop();
		}

        // This is not called when restarting the system ?
        protected override void OnShutdown()
        {
            this.OnStop();
        }
	}
}