/*(c) Copyright 2009, VersionOne, Inc. All rights reserved. (c)*/
using System.Reflection;

[assembly: AssemblyProduct("VersionOne Integration")]
[assembly: AssemblyVersion("8.0.479.0")]
[assembly: AssemblyInformationalVersion("8.0")]
