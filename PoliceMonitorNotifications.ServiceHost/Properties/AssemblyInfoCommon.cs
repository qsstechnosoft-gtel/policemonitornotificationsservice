/*(c) Copyright 2009, VersionOne, Inc. All rights reserved. (c)*/
using System.Reflection;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PoliceMonitor")]
[assembly: AssemblyProduct("PoliceMonitor Notifications Service")]
[assembly: AssemblyCopyright("Copyright © PoliceMonitor 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.*")]

