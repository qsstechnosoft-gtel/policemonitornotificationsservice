using System;
using PoliceMonitorNotifications.ServiceHost.Logging;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
	internal class ServiceMode : CommonMode
	{
        internal void Start()
        {
            try
            {
                Startup(false);
                EventManager.OnRestartRequired += new PoliceMonitorNotifications.ServiceHost.Eventing.RestartRequired(Restart);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceMode_Exception_1"), ex);
            }
        }

        internal void Stop()
        {
            try
            {
                Shutdown();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceMode_Exception_2"), ex);
            }
        }

        private void Restart()
        {
            LogMessage.Log(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ServiceMode_Debug_1"), EventManager);
            // This will automatically Restart the Windows Service. (The Service is set to restart at failure time.)
            Environment.Exit(1);
        }
	}
}