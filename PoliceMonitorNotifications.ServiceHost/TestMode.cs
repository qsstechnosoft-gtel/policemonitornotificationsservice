﻿using System;
using System.Collections.Generic;
using System.Xml;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.Resources;

namespace PoliceMonitorNotifications.ServiceHost
{
    public class TestMode : CommonMode
    {
        public void StartUp(bool testMode)
        {
            base.Startup(testMode); // testMode shoud be 'true'
            EventManager.OnRestartRequired += new PoliceMonitorNotifications.ServiceHost.Eventing.RestartRequired(Restart);
        }

        private void Restart()
        {
            // (Think) Do not restart when executing Unit Tests.
            throw new Exception(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_TestMode_Exception_1"));
        }

        public new IEventManager EventManager
        {
            get
            {
                return base.EventManager;
            }
        }

        public XmlElement EmailNotificationsServiceConfig
        {
            get
            {
                IList<ServiceInfo> services = base.Services;
                foreach (ServiceInfo ss in Services)
                {
                    if (ss.Name == "EmailNotificationsService")
                    {
                        return ss.Config;
                    }
                }
                return null;
            }
        }
    }
}
