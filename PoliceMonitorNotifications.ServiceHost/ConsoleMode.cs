using System;
using PoliceMonitorNotifications.ServiceHost.Resources;


namespace PoliceMonitorNotifications.ServiceHost
{
	internal class ConsoleMode : CommonMode
	{  
		internal void Run()
		{
			try
			{
				InternalRun();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
            
			Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleMode_Debug_1"));
			Console.Read();
		}

        private void Restart()
        {
            //// Get the current Process           
            //System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
            // Start a New Process
            System.Diagnostics.Process.Start(Environment.CurrentDirectory + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            // Exit the Current Process
            Environment.Exit(0);
        }

        
		private void InternalRun()
		{            
			Startup(false);

            EventManager.OnRestartRequired += new PoliceMonitorNotifications.ServiceHost.Eventing.RestartRequired(Restart);

			Console.TreatControlCAsInput = true;
			bool quit = false;
			while (!quit)
			{
				ConsoleKeyInfo info = Console.ReadKey(true);
				switch (info.Key)
				{
					case ConsoleKey.Q:
						quit = true;
						break;
					case ConsoleKey.H:
                        Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleMode_Debug_2"));
                        Console.WriteLine(ResourceManager.GetResourceString("PoliceMonitorNotificationsServiceHost_ConsoleMode_Debug_3"));
						break;
					case ConsoleKey.C:
						if ((info.Modifiers & ConsoleModifiers.Control) == ConsoleModifiers.Control)
							quit = true;
						break;
					default:
						break;
				}
			}

			Shutdown();
		}
	}
}