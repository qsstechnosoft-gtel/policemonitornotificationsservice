﻿using PoliceMonitorNotifications.ServiceHost.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceMonitorNotifications.ServiceHost.EmailNotificationsService
{
    public class Utility
    {
        #region Fields
        static Utility _this = null;
        //DateTime _DT1970 = new DateTime(1970,1,1);
        DateTime _DT1970 = new DateTime(1970, 1, 1);

        int _DisplayHourAdjustment = 0;
        string _BaseURL = "";
        #endregion

        public static Utility Instance
        {
            get
            {
                return _this;
            }
        }
        private Utility()
        {
            this.ValidTalkGroupIDs = new List<int>();
        }

        public static Utility Get(string inBaseIP, string inTalkIDs)
        {
            if (_this == null)
            {
                _this = new Utility();
                _this.SetConfigValues(inBaseIP, inTalkIDs);
            }
            return _this;
        }

        public void SetConfigValues(string inBaseIP, string inTalkIDs)
        {

            List<int> tmpList = new List<int>();

            if (string.IsNullOrEmpty(inTalkIDs) == false)
            {
                int tmpID = 0;
                foreach (string info in inTalkIDs.Split(','))
                {
                    if (info == "") continue;

                    if (int.TryParse(info, out tmpID) == true)
                        tmpList.Add(tmpID);
                }
            }

            this.ValidTalkGroupIDs.AddRange(tmpList.Distinct().OrderBy(c => c));
            this.BaseURL = inBaseIP;
        }

        public List<int> ValidTalkGroupIDs { get; set; }


        public int StartHour { get; set; }
        public int EndHour { get; set; }
        public int MinTranSeconds { get; set; }
        public string BaseURL
        {
            get
            {
                return _BaseURL;
            }
            private set
            {
                if (string.IsNullOrEmpty(value) == true)
                    _BaseURL = "http://";
                else
                {
                    if (value.EndsWith("/") == false)
                        _BaseURL = value + "/";
                    else
                        _BaseURL = value;
                }

                if (_BaseURL.ToLower().StartsWith("http://") == false)
                    _BaseURL = "http://" + _BaseURL;
            }
        }
        public DateTime ConvertJsonDT(Int64 MilliSince1970)
        {

            DateTime myDT = _DT1970.AddMilliseconds(MilliSince1970);
            return myDT;
        }


        public Int64 GetJsonDT(int HoursAgo)
        {
            // Get seconds from 1970 to now

            var DTAgo = DateTime.Now.Subtract(new TimeSpan(HoursAgo, 0, 0));

            var Sec = (Int64)(DTAgo.ToUniversalTime().Subtract(_DT1970)).TotalSeconds;

            return Sec;
        }


        public int StrToInt(string value)
        {
            int Result = 0;
            int.TryParse(value, out Result);
            return Result;
        }

        public List<Radio> RadioCachList { get; set; }
        public List<TalkGroup> TalkGroups { get; set; }
        

    }
}
