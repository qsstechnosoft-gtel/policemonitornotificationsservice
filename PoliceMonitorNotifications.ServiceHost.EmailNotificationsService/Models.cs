﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PoliceMonitorNotifications.ServiceHost.EmailNotificationsService
{
   public class JsonResponseIndexMatch
    {
        public List<List<object>> matches { get; set; }
    }

    public class JsonResponseTalkGroup
    {
        public List<List<object>> talkgroups { get; set; }
    }


    public class JsonResponseRadio
    {
        public string __invalid_name__100003 { get; set; }
        public string __invalid_name__100012 { get; set; }
        public string __invalid_name__100014 { get; set; }
    }

    public class TalkGroup
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class Radio
    {
        public int ID { get; set; }
        public int SystemId { get; set; }
        string _Description;
        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(_Description) == true)
                    return string.Format("[{0}]", this.ID);
                else return _Description;
            }
            set
            {
                _Description = value;
            }
        }
    }
    public class TalkGroupItem
    {
        public string ID { get; set; }
        public Int64 TransStartMSec { get; set; }
        public Int64 TransLengthMSec { get; set; }
        public Int64 RadioID { get; set; }

        public Int64 TalkGroupID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MMM}", ApplyFormatInEditMode = true)]
        public DateTime TransStartUTC
        {
            get
            {
                DateTime MyDT = Utility.Instance.ConvertJsonDT(this.TransStartMSec);
                //string myResult = String.Format("{0:MMM d HH:mm:ss}", MyDT);

                return MyDT;
            }
        }


        public string TransTimeDesc
        {
            get
            {
                if (TransLengthMSec > 0)
                {
                    TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)TransLengthMSec);
                    return string.Format("{0:F1}s", ts.TotalSeconds);
                }
                else
                    return "NA";
            }
        }

        public string RadioDesc { get; set; }



        public string RadioDesc1
        {
            get
            {
                string Result = "";
                if (string.IsNullOrEmpty(RadioDesc) == false)
                {

                    int idx = RadioDesc.IndexOf("(");
                    int idx2 = RadioDesc.IndexOf(")");
                    if (idx > -1 && idx2 > -1 && idx2 > idx)
                    {

                        Result = RadioDesc.Substring(idx + 1, idx2 - (idx + 1));

                        Int32 tmpID = 0;
                        if (int.TryParse(Result, out tmpID) == true)
                            Result = RadioDesc;
                        else
                            Result = "";

                        //Result = "(" + RadioDesc.Substring(idx).Replace("(", "").Replace(")", "") + ")";
                    }
                }

                return Result;
            }



        }
        public string RadioDesc2
        {
            get
            {
                if (string.IsNullOrEmpty(RadioDesc1) == false)
                    return RadioDesc.Replace(this.RadioDesc1, "");
                else
                    return RadioDesc;
            }

        }


    }

}

