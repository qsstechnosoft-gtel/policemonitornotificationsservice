﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using PoliceMonitorNotifications.Profile;
using PoliceMonitorNotifications.ServiceHost.Data;
using PoliceMonitorNotifications.ServiceHost.Eventing;
using PoliceMonitorNotifications.ServiceHost.HostedServices;
using PoliceMonitorNotifications.ServiceHost.Logging;
using PoliceMonitorNotifications.ServiceHost.Mailing;
using System.Linq;
using Newtonsoft.Json;
using PoliceMonitorNotifications.ServiceHost.Resources;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Threading;
using PoliceMonitorNotifications.ServiceHost.Core.Services;
using Newtonsoft.Json.Linq;
using System.Collections;
using PoliceMonitorNotifications.ServiceHost.Core;
using System.Text;
//using PoliceMonitorNotifications.ServiceHost.Core;


namespace PoliceMonitorNotifications.ServiceHost.EmailNotificationsService
{
    
    public class EmailNotificationsHostedService : IHostedServiceTest
    {
        public class IntervalSync { }

        private IProfile _profile;
        private IProfileStore _profileStore;
        private readonly DateTime MIN_DATE = DateTime.Parse("01/01/1970");
        DateTime lastScannedForNewDefects = DateTime.MinValue;
        private IEventManager eventManager;

        private string talkGroupBaseUrl = null;
        private static bool TEST = true;

        private static DateTime lastTimeTalkGroupAndRadioIdFetch = DateTime.Now.ToUniversalTime();

        private static DateTime lastTimeTransmissionArchived = DateTime.Now.ToUniversalTime();

        private DateTime NextRunShrink = DateTime.Now.ToUniversalTime();
        
        private static DateTime lastTimeDateTimeSync = DateTime.Now.ToUniversalTime();

        //in second
        private int servicePollInterval = 0;

        private int serverTimeDiffInSeconds = 0;

        private int timeDiffInSeconds = 0;
        private int configServerTimeDiffInSeconds = 0;

        private static List<CustomerSettingCase> CustomersSettingCases = new List<CustomerSettingCase>();

        public void Initialize(XmlElement config, IEventManager evtManager, object param, bool testMode)
        {
            _profileStore = new XmlProfileStore("profile.xml");
            _profile = _profileStore[config.Name];

            eventManager = evtManager;

            talkGroupBaseUrl = config["TalkGroupBaseUrl"].InnerText;
            configServerTimeDiffInSeconds = Convert.ToInt32(config["ServerTimeDiffInSeconds"].InnerText);
            serverTimeDiffInSeconds = configServerTimeDiffInSeconds;
            Utility.Get(talkGroupBaseUrl, null);

            servicePollInterval = Convert.ToInt32(TimePublisherService._interval);

            // link events
            PoolTalkGroup();
            //evtManager.Subscribe(typeof(IntervalSync), PoolTalkGroup);

            //OnStart Up Calcution should start from current time
            //DateTime nextV1Scan = DateTime.Now.ToUniversalTime();
            //DateTime NtpDateTime = NtpClient.GetNetworkTime().ToUniversalTime();
            //LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("nextV1Scan ToUniversalTime {0}", nextV1Scan.ToString()), eventManager);
            //LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("NtpDateTime ToUniversalTime {0}", NtpDateTime.ToString()), eventManager);
            //timeDiffInSeconds = nextV1Scan.Ticks > NtpDateTime.Ticks ? -Convert.ToInt32((nextV1Scan - NtpDateTime).TotalSeconds) : Convert.ToInt32((NtpDateTime - nextV1Scan).TotalSeconds);
            //LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Time Diff In Seconds {0}", timeDiffInSeconds.ToString()), eventManager);
            //serverTimeDiffInSeconds = serverTimeDiffInSeconds + (timeDiffInSeconds);

            //nextV1Scan = nextV1Scan.AddSeconds(serverTimeDiffInSeconds);
            //SetLastScannedTimestampUTC(nextV1Scan, null, null);

            //var listCustomers = SqlServerDao.GetAllCustomers().Result;
            //foreach (var cust in listCustomers)
            //{
            //    SqlServerDao.SetLastScannedTimestampUTC(cust.CustomerId, nextV1Scan);
            //}

        }

        /// <summary>
        /// Timer interval on which to poll TalkGroup WS. See app config file for time in milliseconds.
        /// </summary>
        /// <param name="pubobj">Not used</param>
        private async void PoolTalkGroup()
        {
            //DateTime lastScanned = GetLastScannedTimestamp(null, null);
            //LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Start Poll Service from {0}", lastScanned.ToString()), eventManager);
            //SetLastScannedTimestampUTC(nextV1Scan, null, null);
            //Task.Factory.StartNew(async () => { await PollTalkGroups(nextV1Scan); });

            if(!TEST)
                await GetLatestRadiosAndTalkgroups();

            DateTime nextV1Scan = DateTime.Now.ToUniversalTime();
            nextV1Scan = nextV1Scan.AddSeconds(serverTimeDiffInSeconds);

            int counter = 0;
            while (true)
            {
                if (!TEST && (counter * (servicePollInterval / 1000)) / 2000 > 1)
                {
                    await GetLatestRadiosAndTalkgroups();
                    counter = 0;
                }

                if (counter % 20 == 0)
                {
                    GetPoolTalkgroups();
                    GetPoolRadios();
                }

                if (counter % 20 == 0)
                {
                    await NotificationSettingCases();
                    //Task.Factory.StartNew(async () => { await SendNotificationToCustomers(); });
                }

                counter++;

                //if (DateTime.Now.ToUniversalTime() >= NextRunShrink.AddHours(1))
                //{
                //     ShrinkTable();
                //}

                //TimeSpan start = new TimeSpan(1, 0, 0); //1 o'clock
                //TimeSpan end = new TimeSpan(1, 15, 0); //1:15 o'clock
                //TimeSpan now = DateTime.Now.TimeOfDay;

                //if ((now > start) && (now < end))
                //{
                //  //  ShrinkTable();
                //}


                foreach (int systemId in poolTalkgroups.Keys)
                {
                    ////if (TEST && systemId != 16960)
                    ////{
                    ////    continue;
                    ////}
                    await PollTalkGroups(poolTalkgroups[systemId], poolTalkgroupsArray[systemId], systemId, nextV1Scan);
                    ////Task.Factory.StartNew(async () => { await SendNotificationToCustomers(); });
                }
                //Thread.Sleep(servicePollInterval-5000);
            }
            
            // FIXME
            //Task.Factory.StartNew(async () => { await SendNotificationToCustomers(nextV1Scan); });
            
            //SendNotificationToCustomers(nextV1Scan);
            //Task.Factory.StartNew(async () =>
            //{
                // FIXME: Run this
                //if (nextV1Scan.Subtract(lastTimeTransmissionArchived).TotalHours >= 24)
                //{
                //    lastTimeTransmissionArchived = nextV1Scan;
                //    await Archive_PolledTalkGroupItem(nextV1Scan);
                //}

                /*
                if (nextV1Scan.Subtract(lastTimeDateTimeSync).TotalMinutes >= 5)
                {
                    DateTime currentSystemDateTime = DateTime.Now.ToUniversalTime();
                    lastTimeDateTimeSync = currentSystemDateTime;
                    DateTime NtpDateTime = NtpClient.GetNetworkTime().ToUniversalTime();
                    int tempTimeDiffInSeconds = currentSystemDateTime.Ticks > NtpDateTime.Ticks ? -Convert.ToInt32((currentSystemDateTime - NtpDateTime).TotalSeconds) : 
                        Convert.ToInt32((NtpDateTime - currentSystemDateTime).TotalSeconds);

                    if (tempTimeDiffInSeconds >= timeDiffInSeconds)
                    {
                        if ((tempTimeDiffInSeconds - timeDiffInSeconds) > 5)
                        {
                            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Time Diff In Seconds {0}", tempTimeDiffInSeconds.ToString()), eventManager);
                            serverTimeDiffInSeconds = configServerTimeDiffInSeconds + (Convert.ToInt32(tempTimeDiffInSeconds));
                        }
                    }
                    else
                    {
                        if ((timeDiffInSeconds - tempTimeDiffInSeconds) > 5)
                        {
                            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Time Diff In Seconds {0}", tempTimeDiffInSeconds.ToString()), eventManager);
                            serverTimeDiffInSeconds = configServerTimeDiffInSeconds + (Convert.ToInt32(tempTimeDiffInSeconds));
                        }
                    }
                }*/
                
            //});
        }

        Dictionary<int, string> poolTalkgroups = new Dictionary<int, string>();
        Dictionary<int, List<Data.TalkGroup>> poolTalkgroupsArray = new Dictionary<int, List<Data.TalkGroup>>();
        List<Data.Radio> poolRadiosArray = new List<Data.Radio>();

       


        ////funtion use to remove data form PolledTalkGroupItem table



        //private void ShrinkTable()
        //{
        //    using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
        //    {
        //        LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Remove data from table PolledTalkGroupItem start Time {0}", DateTime.Now), eventManager);
        //        var MaxID = db.PolledTalkGroupItems.Max(m => m.ID);
        //        var count = db.PolledTalkGroupItems.Count();
        //        if (count > 350000)
        //        {
        //            List<PolledTalkGroupItem> data = db.PolledTalkGroupItems.Where(w => w.ID < (MaxID - 350000)).ToList();
        //            db.PolledTalkGroupItems.RemoveRange(data);
        //            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Remove data from table PolledTalkGroupItem less than ID {0} Time {1}", MaxID - 350000, DateTime.Now), eventManager);
        //            db.SaveChanges();
        //            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Remove data from table PolledTalkGroupItem succesfully.End Time {0}!!", DateTime.Now), eventManager);
        //        }
        //        //NextRunShrink = DateTime.Now.ToUniversalTime();
        //    }
        //}
        private void ShrinkTable()
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Remove data from table PolledTalkGroupItem start Time {0}", DateTime.Now), eventManager);
                var MinValue = db.DeleteTalkgroup();
                LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Remove data from table PolledTalkGroupItem succesfully.End Time {0}!!", DateTime.Now), eventManager);
            }
        }



        /// <summary>
        /// This function will return the talkgroups which need to be pooled from CGI for transmissions
        /// </summary>
        /// 

        private void GetPoolTalkgroups()
        {
            Dictionary<Int32, List<Data.TalkGroup>> distinctTalkGroups = SqlServerDao.GetAllDistinctTalkGroups();

            poolTalkgroups = new Dictionary<int, string>();
            poolTalkgroupsArray = new Dictionary<int, List<Data.TalkGroup>>();

            foreach(int systemId in distinctTalkGroups.Keys)
            {
                List<Data.TalkGroup> talkgroups = distinctTalkGroups[systemId];
                string tkgroups = "";
                foreach (Data.TalkGroup talkgroup in talkgroups)
                {
                    tkgroups += talkgroup.ID + ",";
                }
                if (!tkgroups.Equals(""))
                {
                    tkgroups = tkgroups.Substring(0, tkgroups.Length - 1);
                }
                poolTalkgroups.Add(systemId, tkgroups);
                poolTalkgroupsArray.Add(systemId, talkgroups);
            }
        }
        /// <summary>
        /// This function will return the talkgroups which need to be pooled from CGI for transmissions
        /// </summary>
        private void GetPoolRadios()
        {
            poolRadiosArray = SqlServerDao.GetAllRadios();
        }

        System.Collections.Hashtable tkgroups = new System.Collections.Hashtable();
        string talkgroups = "";
        private Hashtable maxItemID1 = new Hashtable();
        string maxItemID = "";

        private async Task GetLatestRadiosAndTalkgroups()
        {
            await GetSystems();
            await GetTalkGroups();
            await GetRadioIDs();
        }

        List<Data.TblSystem> systems;

        private async Task GetSystems()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(talkGroupBaseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = new TimeSpan(0, 59, 0);
                // New code:
                HttpResponseMessage reaponselist = await client.GetAsync("radio/list_systems.cgi");

                if (reaponselist.IsSuccessStatusCode)
                {
                    var josnlist = await reaponselist.Content.ReadAsStringAsync();
                    systems = new List<Data.TblSystem>();
                    var parsjasonlist = JObject.Parse(josnlist);
                    foreach (var item in parsjasonlist)
                    {
                        Data.TblSystem s = new Data.TblSystem();
                        s.ID = Convert.ToInt32(item.Key);
                        s.Name = (String)item.Value["name"];
                        systems.Add(s);
                    }

                    if (systems.Count > 0)
                    {
                        await SqlServerDao.Savesystem(systems);
                    }
                }
            }
        }

        private async Task GetTalkGroups()
        {
            try
            {
                lastTimeTalkGroupAndRadioIdFetch = DateTime.Now;
                lastTimeTalkGroupAndRadioIdFetch = lastTimeTalkGroupAndRadioIdFetch.AddSeconds(serverTimeDiffInSeconds);

                Utility.Instance.TalkGroups = new List<TalkGroup>();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(talkGroupBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 59, 0);
                    {
                        int Cnt = -1;
                        List<Data.TalkGroup> talkGroupList = new List<Data.TalkGroup>();
                        foreach (var item in systems)
                        {
                            HttpResponseMessage response = await client.GetAsync("radio/list_talkgroups.cgi?system_id=" + item.ID);
                            Cnt = -1;
                            if (response.IsSuccessStatusCode)
                            {
                                var json = await response.Content.ReadAsStringAsync();
                                var respItem = JsonConvert.DeserializeObject<JsonResponseTalkGroup>(json);
                                Utility.Instance.TalkGroups.Clear();
                                   
                                foreach (var info in respItem.talkgroups)
                                {

                                    Cnt++;

                                    int ItemCnt = info.Count;
                                    if (ItemCnt > 1 && Utility.Instance.ValidTalkGroupIDs.Exists(c => c == Cnt) == true)
                                    {
                                        string tmp = " (" + (string)info[2] + ") " + (string)info[1];
                                        Utility.Instance.TalkGroups.Add(new TalkGroup() { Value = Cnt.ToString(), Text = string.Format("{0}", tmp) });
                                    }

                                    if (ItemCnt > 1)
                                    {
                                        Data.TalkGroup tg = new Data.TalkGroup();
                                        tg.ID = Cnt;
                                        tg.TalkgroupID = (string)info[0];
                                        tg.Name = (string)info[1];
                                        tg.County = (string)info[2];
                                        tg.ServerUrl = Convert.ToString(client.BaseAddress);
                                        tg.SystemId = Convert.ToInt32(item.ID);
                                        talkGroupList.Add(tg);
                                    }
                                }

                            }
                        }

                        if (talkGroupList.Count > 0)
                        {
                            SqlServerDao.SaveTalkGroups(talkGroupList, true);
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage.Log(LogMessage.SeverityType.Error, ex.Message, eventManager);
            }
        }

        private async Task GetRadioIDs()
        {
            try
            {
                {
                    //LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("GetRadioIDs {0}", DateTime.Now.Subtract(lastTimeTalkGroupAndRadioIdFetch).Minutes.ToString()), eventManager);

                    Utility.Instance.RadioCachList = new List<Radio>();
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Utility.Instance.BaseURL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.Timeout = new TimeSpan(0, 59, 0);
                        // New code:

                        List<TblSystem> systems = SqlServerDao.GetSystemsList();

                        List<Data.Radio> radioList = new List<Data.Radio>();
                        foreach (TblSystem itemlist in systems)
                        {
                            HttpResponseMessage response = await client.GetAsync("radio/list_radios.cgi?system_id=" + itemlist.ID);
                            if (response.IsSuccessStatusCode)
                            {
                                var json = await response.Content.ReadAsStringAsync();
                                //Insert data in table Radios

                                var parseJson = JObject.Parse(json);

                                foreach (var item in parseJson)
                                {
                                    Data.Radio r = new Data.Radio();
                                    if (item.Key != Convert.ToString(0))
                                    {
                                        r.ID = Convert.ToInt32(item.Key);
                                        r.Description = Convert.ToString(item.Value);
                                        r.SystemId = itemlist.ID;
                                        radioList.Add(r);
                                    }
                                }
                            }
                        }
                        if (radioList.Count > 0)
                        {
                            SqlServerDao.SaveRadios(radioList, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage.Log(LogMessage.SeverityType.Error, ex.Message, eventManager);
            }
        }

        private async Task PollTalkGroups(string tgQuery, List<Data.TalkGroup> talkgroups, int systemId, DateTime transStartUTC)
        {
            try
            {
                //LogMessage.Log(LogMessage.SeverityType.Debug, string.Format("Start Scanning Transmission for TalkGroup {0} from {1} ", tgQuery, transStartUTC.ToString()), eventManager);
                List<Tuple<PolledTalkGroupItem, string>> tmpList = new List<Tuple<PolledTalkGroupItem, string>>();
                List<PolledTalkGroupItem> WorkingList = new List<PolledTalkGroupItem>();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(talkGroupBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 10, 0);
                    if(TEST)
                    {
                        String username = "admin";
                        String password = "P5zHfKBpj3";
                        String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                        client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
                    }

                    string URLSearch = string.Format("radio/index_search.cgi?system_id={0}&tg={1}", systemId, tgQuery);
                    DateTime fromTimeUTC = transStartUTC;

                    if (maxItemID1.Contains(systemId))
                    {
                        if (maxItemID1[systemId].ToString() != "")
                        {
                            maxItemID = maxItemID1[systemId].ToString();

                        }
                    }
                    /**********************************************/
                    if (TEST)
                    {
                        //maxItemID = "857714";
                    }
                    /**********************************************/
                    if (maxItemID.Equals(""))
                    {
                        using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                        {
                            long max = db.PolledTalkGroupItems.Where(x => x.SystemID == systemId).DefaultIfEmpty().Max(r => r == null ? 0 : r.ID);
                            //LogMessage.Log(LogMessage.SeverityType.Debug, "MaxID:  " + max, eventManager);
                            if(max != 0)
                            {
                                maxItemID = db.PolledTalkGroupItems.Where(p => p.ID == max).First().TalkGroupItemId;
                            }
                        }
                    }
                    if (maxItemID.Equals(""))
                    {
                        //LogMessage.Log(LogMessage.SeverityType.Debug, "MaxID:  0", eventManager);
                        Int64 StartAgo = (Int64)(fromTimeUTC.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        if (StartAgo > 0)
                        {
                            URLSearch += string.Format("&start_time={0}", StartAgo);
                        }
                    }
                    else
                    {
                        //LogMessage.Log(LogMessage.SeverityType.Debug, "MaxIDItem:  " + maxItemID, eventManager);
                        URLSearch += string.Format("&after={0}", maxItemID);
                    }


                    //LogMessage.Log(LogMessage.SeverityType.Debug, string.Format(" {0}{1}   ", talkGroupBaseUrl, URLSearch), eventManager);
                    // New code:
                    HttpResponseMessage response = await client.GetAsync(URLSearch);

                    if (response.IsSuccessStatusCode == false)
                    {
                        //LogMessage.Log(LogMessage.SeverityType.Error, string.Format("URLSearch {0} ReasonPhrase {1}  ", URLSearch, response.ReasonPhrase), eventManager);
                        throw new Exception( response.ReasonPhrase);
                    }
                    else
                    {
                        var json = await response.Content.ReadAsStringAsync();
                        var respItem = JsonConvert.DeserializeObject<JsonResponseIndexMatch>(json);

                        foreach (var info in respItem.matches)
                        {
                            int Cnt = -1;
                            PolledTalkGroupItem item = new PolledTalkGroupItem();
                            item.RawTransmission = JsonConvert.SerializeObject(info);
                            item.SystemID = systemId;
                            foreach (Object col in info)
                            {
                                Cnt++;
                                if (Cnt == 0)
                                    item.TalkGroupItemId = (string)col;
                                else if (Cnt == 1)
                                    item.TransStartMSec = (long)col;
                                else if (Cnt == 2)
                                    item.TransLengthMSec = (long)col;
                                else if (Cnt == 3)
                                    item.TalkGroupID = (long)col;
                                else if (Cnt == 6)
                                    item.RadioID = (long)col;
                                else if(Cnt == 10)
                                {
                                    JObject obj = (Newtonsoft.Json.Linq.JObject)(col);
                                    JToken token;
                                    obj.TryGetValue("tones", out token);
                                    if(token != null)
                                    {
                                        Newtonsoft.Json.Linq.JArray arr = token as Newtonsoft.Json.Linq.JArray;
                                        List<JToken> l = arr.ToList();
                                        if(l.Count > 0)
                                        {
                                            foreach (JToken t in l)
                                            {
                                                item.Tones += t.ToString() + ", ";
                                            }

                                            item.Tones = item.Tones.Substring(0, item.Tones.Length - 2);
                                        }
                                    }
                                }
                            }

                            tmpList.Add(new Tuple<PolledTalkGroupItem, string>(item, string.Format("{0}{1}{2}", item.TransStartMSec, item.TransLengthMSec, item.RadioID)));
                        }

                        string LastUniqueID = "";

                        //if(tmpList.Count>0)
                        //{
                        //    LogMessage.Log(LogMessage.SeverityType.Debug, string.Format("Start Scanning Transmission for TalkGroup {0} from {1} to {2} ", tgQuery, transStartUTC.ToString(), transEndUTC.ToString()), eventManager);
                        //    LogMessage.Log(LogMessage.SeverityType.Debug, string.Format("URLSearch {0} Count {1}  ", URLSearch, tmpList.Count), eventManager);
                        //}
                        foreach (var c in tmpList)
                        {
                            if (LastUniqueID == c.Item2)
                                continue;
                            else
                            {
                                WorkingList.Add(c.Item1);
                                LastUniqueID = c.Item2;
                            }
                        }

                    }
                }

                var UpdateDesc = from c in WorkingList
                                 join d in talkgroups on c.TalkGroupID equals d.ID
                                 select new Tuple<PolledTalkGroupItem, Data.TalkGroup>(c, d);
                                 
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    foreach (var info in UpdateDesc.ToList())
                    {
                        string tmpRadioDesc = "";
                        string RadioNum = info.Item1.RadioID.ToString();

                        var myRadio = poolRadiosArray.FirstOrDefault(c => c.ID == info.Item1.RadioID && c.SystemId == systemId);
                        if (myRadio != null)
                        {
                            tmpRadioDesc = myRadio.Description;
                        }
                        string ChannelDesc = "(" + info.Item2.County + ") " + info.Item2.Name;// info.Item2.TalkgroupID;// + countys[0] 

                        if (tmpRadioDesc.ToUpper().IndexOf("DISPATCH") > -1)
                        {
                            info.Item1.RadioDesc = "DISPATCH:::" + ChannelDesc;
                        }
                        else
                        {
                            if(info.Item1.RadioID == 0)
                            {
                                info.Item1.RadioDesc = "TONES" + (string.IsNullOrEmpty(info.Item1.Tones) ? "" : ": " + info.Item1.Tones);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tmpRadioDesc))
                                {
                                    info.Item1.RadioDesc = string.Format("{0} ({1})", tmpRadioDesc, RadioNum);
                                }
                                else
                                {
                                    info.Item1.RadioDesc = string.Format("{0} ({1})", ChannelDesc, RadioNum);
                                }
                            }
                        }

                        Data.TalkGroup tt = poolTalkgroupsArray[systemId].Where(x => x.ID == info.Item1.TalkGroupID).FirstOrDefault();
                        info.Item1.ChannelName = "(" + tt.County + ") " + tt.Name;// (string)tkgroups["" + info.Item1.TalkGroupID];
                        info.Item1.SystemID = info.Item1.SystemID;
                        info.Item1.TransStartUTC = Utility.Instance.ConvertJsonDT(info.Item1.TransStartMSec.Value);
                        info.Item1.TransTimeDesc = TransTimeDesc(info.Item1.TransLengthMSec.Value);

                    }
            }
                
                List<PolledTalkGroupItem> FilteredList = new List<PolledTalkGroupItem>();

                foreach (var info in UpdateDesc.ToList())
                {
                    FilteredList.Add(info.Item1);

                    if (maxItemID1.Contains(systemId))
                    {
                        maxItemID1.Remove(systemId);
                        maxItemID1.Add(systemId, info.Item1.TalkGroupItemId);
                    }
                    else
                        maxItemID1.Add(systemId, info.Item1.TalkGroupItemId);



                    //maxItemID = info.Item1.TalkGroupItemId;
                }

                if (FilteredList.Count > 0)
                {
                    Console.WriteLine("Found {0}", FilteredList.Count);
                    //Console.WriteLine("Found {0} for TalkGroup {1}", FilteredList.Count, tgQuery);
                    //LogMessage.Log(LogMessage.SeverityType.Debug, string.Format("Found {0} for TalkGroup {1}", FilteredList.Count, tgQuery), eventManager);
                    Dictionary<string, List<PolledTalkGroupItem>> NotificationTalkgroup = new Dictionary<string, List<PolledTalkGroupItem>>();
                    foreach (var talkgroup in FilteredList)
                    {
                        string key = talkgroup.SystemID + "_" + talkgroup.TalkGroupID;
                        
                        if (NotificationTalkgroup.ContainsKey(key))
                        {
                            List<PolledTalkGroupItem> items = NotificationTalkgroup[key];
                            items.Add(talkgroup);
                        }
                        else
                        {
                            List<PolledTalkGroupItem> items = new List<PolledTalkGroupItem>();
                            items.Add(talkgroup);
                            NotificationTalkgroup.Add(key, items);
                        }
                    }
                    //Thread Mythread = new Thread(new ThreadStart(SendNotificationForTalkGroup(NotificationTalkgroup)));
                   
                    maxItemID = "";
                    await SqlServerDao.SaveTransmissions(FilteredList);
                    await SendNotificationForTalkGroup(NotificationTalkgroup);
                    //foreach (var talkGroupItem in FilteredList.ToList())
                    //{
                    //    await SqlServerDao.SaveTransmissions(talkGroupItem.ID, talkGroupItem.TransStartMSec, talkGroupItem.TransLengthMSec, talkGroupItem.RadioID,
                    //        talkGroupItem.TalkGroupID, talkGroupItem.TransStartUTC, talkGroupItem.TransTimeDesc, talkGroupItem.RadioDesc, (string)tkgroups["" + talkGroupItem.TalkGroupID]);
                    //}
                }
            }
            catch(HttpRequestException ex)
            {
                LogMessage.Log(LogMessage.SeverityType.Error,"ffffff", eventManager);
                LogMessage.Log(LogMessage.SeverityType.Error, ex.InnerException.Source, eventManager);
                if (ex.InnerException!=null && ex.InnerException.Message.Equals("Unable to connect to the remote server"))
                {
                    LogMessage.Log(LogMessage.SeverityType.Error, ex.InnerException.Message, eventManager);
                    //QueryTalkGroupForTransmission(tgQuery, tkgroups, transStartUTC);
                }
            }
            catch (Exception ex)
            {
                LogMessage.Log(LogMessage.SeverityType.Error, ex.Message, eventManager);

            }
        }

        public string TransTimeDesc(long TransLengthMSec)
        {
            if (TransLengthMSec > 0)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)TransLengthMSec);
                return string.Format("{0:F1}s", ts.TotalSeconds);
            }
            else
                return "NA";
        }

        Dictionary<string, List<CustomerModel>> NotificationCases = new Dictionary<string, List<CustomerModel>>();
        Dictionary<string, List<PolledTalkGroupItem>> NotificationCasesTalkGroup = new Dictionary<string, List<PolledTalkGroupItem>>();
        List<PolledTalkGroupItem> PolledTalkList = new List<PolledTalkGroupItem>();
      //  List<NotificationSettingRadioMap> CustomerRadios = new List<NotificationSettingRadioMap>();
        List<NotificationSettingRadio> CustomerRadios;

        //public  void SendNotificationForTalkGroup(Dictionary<string, PolledTalkGroupItem> FilteredList)
        private async Task SendNotificationForTalkGroup(Dictionary<string, List<PolledTalkGroupItem>> NotificationTalkgroup)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var lastScaned = db.NotificationSettingTalkGroups.ToList();
                    //var LastScaned = db.CustomerNotificationLastScans.Select(s => new { s.CustomerId, s.Id, s.LastScanUniversalDateTime, s.NotificationName }).ToList();
                    //Check if there is any transmission matching the notification criteria
                    foreach(var talkgroup in NotificationTalkgroup)
                    {
                        if (!NotificationCases.ContainsKey(talkgroup.Key))
                            continue;
                        // get the list of customer to which notification need to be sent
                        var customersForNotifications = NotificationCases[talkgroup.Key];

                        List<PolledTalkGroupItem> transmissions = talkgroup.Value;
                        transmissions = (transmissions == null) ? new List<PolledTalkGroupItem>() : transmissions;
                        transmissions.Reverse();
                        foreach(PolledTalkGroupItem transmission in transmissions)
                        {
                            Console.WriteLine("Transmission received for talkgroup " + talkgroup.Key + " ID:" + transmission.TalkGroupItemId);

                            if (customersForNotifications != null)
                            {
                                //var data = NotificationTalkgroup[talkgroup.Key];
                                foreach (var notify in customersForNotifications)
                                {
                                    if(notify == null)
                                    {
                                        continue;
                                    }

                                    string key = talkgroup.Key;
                                    var notificationId = notify.CustomerNotificationSetting.CustomerNotificationId;
                                    var talk = key.Split('_').Select(x => Int32.Parse(x)).ToList();
                                
                                    //var lastScantime = await SqlServerDao.GetLastScannedTime(talk, notificationId);

                                    int talkid = Convert.ToInt32(talk[1]);
                                    int sysid = Convert.ToInt32(talk[0]);

                                    // var lastScantime = lastScaned.Where(x => x.CustomerNotificationId == notificationId && x.SystemID == sysid && x.TakGroupId == talkid).Select.FirstOrDefault();
                                    var lastScanDate = lastScaned.Where(x => x.CustomerNotificationId == notificationId && x.SystemID == sysid && x.TakGroupId == talkid).FirstOrDefault();
                                    var lastScantime =   Convert.ToDateTime(lastScanDate.LastScanUniversalDateTime);
                                    //var dateTimeNow = DateTime.Now.ToUniversalTime();
                                    var dateTimeNow = Convert.ToDateTime(transmission.TransStartUTC);

                                    if(lastScanDate.LastScanUniversalDateTime==null)
                                    {
                                        await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow);
                                        continue;
                                    }

                                    if (lastScantime > dateTimeNow)
                                    {
                                        continue;
                                    }
                                    var diffInSeconds = ((dateTimeNow - lastScantime).TotalSeconds);
                                    //Console.WriteLine("Last notification sent on " + lastScantime + " (curr time: " + dateTimeNow + ", diff: " + diffInSeconds);

                                    var datetime = dateTimeNow.AddSeconds(-(diffInSeconds));
                                    var talkg = Convert.ToInt64(talk[1]);
                                    var sysg = Convert.ToInt32(talk[0]);
                                    PolledTalkList = db.PolledTalkGroupItems.Where(w => w.SystemID == sysg && w.TalkGroupID == talkg && w.TransStartUTC > datetime).ToList();
                                    if(PolledTalkList.Count > 0)
                                    {

                                    }
                                    else
                                    {
                                        continue;
                                    }
                                    //Case 1
                                    if (notify.CustomerNotificationSetting != null && notify.CustomerNotificationSetting.Interval != null && notify.CustomerNotificationSetting.NotifiyTransmissionsCount != null)
                                       // && diffInSeconds >= notify.CustomerNotificationSetting.Interval)
                                    {
                                        await Case1(transmission, notify, notificationId, talk, lastScantime, dateTimeNow);
                                        lastScaned = db.NotificationSettingTalkGroups.ToList();
                                    }



                                    //Case 2 (Notify on first transmission and no further notification till 1 hour(config second)
                                    if (notify.CustomerNotificationSetting != null && notify.CustomerNotificationSetting.AfterFirstTransmissionWaitInterval != null && notify.CustomerNotificationSetting.AfterFirstTransmissionWaitInterval <= diffInSeconds)
                                    {
                                        await Case2(transmission, notify, notificationId, talk, lastScantime, dateTimeNow);
                                        lastScaned = db.NotificationSettingTalkGroups.ToList();
                                    }


                                    //Case 3 Last X seconds if Y transmisison then notify and not notify till  N sec
                                    if (notify.CustomerNotificationSetting != null && notify.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval != null
                                    && notify.CustomerNotificationSetting.LastNSecInterval != null && notify.CustomerNotificationSetting.LastNSecTransmissionsCount != null)
                                        //&& diffInSeconds >= notify.CustomerNotificationSetting.LastNSecInterval)
                                    {
                                        await case3(transmission, notify, notificationId, talk, lastScantime, dateTimeNow);
                                        lastScaned = db.NotificationSettingTalkGroups.ToList();
                                    }
                                }
                            }
                            break;
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage.Log(LogMessage.SeverityType.Error, ex.Message, eventManager);
            }
        }

        //private async Task case3(PolledTalkGroupItem transmission, CustomerModel notify, int notificationId, List<int> talk, DateTime lastScantime, DateTime dateTimeNow)
        //{
        //        //await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow);
        //        var radioID = CustomerRadios.Where(w => w.CustomerId == notify.CustomerId && w.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s => s.RadioId).ToList();
        //        int flag = 0;
        //        foreach (var radio in radioID)
        //        {
        //            if (radio != 0)
        //            {
        //                if (transmission.RadioID != radio)
        //                {
        //                    flag++;
        //                }
        //            }
        //        }
        //        if (flag > 0)
        //        {
        //            return;
        //        }
        //        //var FilteredList1 = PolledTalkList.Where(w => w.SystemID == talk[0] && w.TalkGroupID == talk[1] && w.TransStartUTC >= lastScantime).ToList();
        //        var lastSaveSetting = CustomersSettingCases.Where(x => x.CustomerId == notify.CustomerId && x.SystemID == talk[0] && x.TalkGroupId == talk[1] && x.CustomerSettingCaseName == "3").FirstOrDefault();
        //        if (lastSaveSetting == null)
        //        {
        //            lastSaveSetting = new CustomerSettingCase()
        //            {
        //                CustomerId = notify.CustomerId,
        //                TalkGroupId = talk[1],
        //                NextScanUniversalDateTime = dateTimeNow,
        //                CustomerSettingCaseName = "3",
        //                SystemID = Convert.ToInt32(talk[0])
        //            };
        //            CustomersSettingCases.Add(lastSaveSetting);
        //        }

        //        dateTimeNow = dateTimeNow.AddSeconds(-Convert.ToInt32(notify.CustomerNotificationSetting.LastNSecInterval));
        //        var FilteredList1 = PolledTalkList.Where(w => w.SystemID == talk[0] && w.TalkGroupID == talk[1] && w.TransStartUTC > dateTimeNow).ToList();
        //        if (FilteredList1.Count > 0 && FilteredList1.Count >= notify.CustomerNotificationSetting.LastNSecTransmissionsCount && dateTimeNow >= lastSaveSetting.NextScanUniversalDateTime)
        //        {
        //            //await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow);
        //            await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow.AddSeconds(Convert.ToInt32(notify.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval)));
        //            CustomersSettingCases.Where(x => x.CustomerId == notify.CustomerId && x.TalkGroupId == talk[1] && x.SystemID == talk[0] && x.CustomerSettingCaseName == "3").
        //                ToList().ForEach(x => x.NextScanUniversalDateTime = dateTimeNow.AddSeconds(Convert.ToInt32(notify.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval)));

        //            string channelName = FilteredList1.First().ChannelName;
        //            string radioWithTransmission = string.Empty;
        //            string radioWithTransmissionInOrder = string.Join(",", FilteredList1.OrderBy(x => x.TransStartUTC).Select(x => x.TalkGroupItemId).FirstOrDefault());
        //            byte[] fileByteArray = new byte[1];
        //            transmission.TalkGroupItemId = "";
        //            foreach (var trasn in FilteredList1)//.GroupBy(x => x.RadioID).Select(grp => new { RadioID = grp.Key, TalkGroupItems = grp.ToList() }).ToList())
        //            {
        //                //foreach (var trasn in transmission1.TalkGroupItems)
        //                {
        //                    radioWithTransmission = radioWithTransmission + String.Format("<br/><br/> <b>Agency Name:</b> {0}  <br/> Transmission play url : <a href='http://rtc.radio1033.com/radio/get_audio.cgi?chunk={1}'>Play</a><br/> Transmission IDs: {1}", trasn.RadioDesc,
        //                        string.Join(",", trasn.TalkGroupItemId));//Select(x => x.TalkGroupItemId).FirstOrDefault()));
        //                    transmission.TalkGroupItemId = transmission.TalkGroupItemId + "" + trasn.TalkGroupItemId + ",";
        //                }
        //            }
        //            transmission.TalkGroupItemId = transmission.TalkGroupItemId.Remove(transmission.TalkGroupItemId.Length - 1, 1);
        //            var msg = String.Format("<b>Channel Name:</b>{0}<br/><b>Notification Name : {2} </b> <br/> {1}  ",
        //                channelName, radioWithTransmission, notify.CustomerNotificationSetting.NotificationName);
        //            var subject = "Transmission Notification for N transmissions in interval with wait";
        //            List<string> userList = new List<string>();
        //            List<int> userIdList = new List<int>();
        //            var users = CusMap.Join(UserList, cm => cm.UserId, ul => ul.UserId, (cm, ul) => new { cm, ul })
        //                .Where(w => w.cm.Active == true && w.cm.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId)
        //                .Select(s => new { s.ul.UserId, s.ul.Email }).ToList();
        //            foreach (var user in users)
        //            {
        //                userList.Add(user.Email.ToString());
        //                userIdList.Add(user.UserId);
        //                LogMessage.Log(LogMessage.SeverityType.Debug, string.Format("Mail sent to {0}", user.Email.ToString()), eventManager);
        //            }
        //            string[] to = userList.Distinct().ToArray();
        //            if (((to == null || to.Length == 0)))
        //            {
        //                return;
        //            }
        //            SendEmailNotifications(transmission, msg, subject, to);
        //            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully for Case 3!!"), eventManager);
        //        }
        //        //else if (FilteredList1.Count > 0 && FilteredList1.Count >= notify.CustomerNotificationSetting.LastNSecTransmissionsCount)//Reset the time
        //        //{
        //        //    CustomersSettingCases.Where(x => x.CustomerId == notify.CustomerId && x.TalkGroupId == talk[1] && x.SystemID == talk[0] && x.CustomerSettingCaseName == "3").ToList().
        //        //       ForEach(x => x.NextScanUniversalDateTime = dateTimeNow.AddSeconds(Convert.ToInt32(notify.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval)));
        //        //}
            
        //}



        private async Task case3(PolledTalkGroupItem transmission, CustomerModel notify, int notificationId, List<int> talk, DateTime lastScantime, DateTime dateTimeNow)
        {
            var  intList= CustomerRadios.Where(w => w.CustomerId == notify.CustomerId && w.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s =>s.RadioId).ToList();
            List<long> radioID = intList.Select(x => (long)x).ToList();
            int flag = 0;
            foreach (var radio in radioID)
            {
                PolledTalkList = PolledTalkList.Where(w => radioID.Contains(w.RadioID.Value) && w.SystemID == talk[0] && w.TalkGroupID == talk[1] && w.TransStartUTC > dateTimeNow).ToList();
                if (radio != 0)
                {
                    if (transmission.RadioID != radio)
                    {
                        flag++;
                    }
                }
            }
            //await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow);
            if (flag > 0)
            {
                return;
            }
            dateTimeNow = dateTimeNow.AddSeconds(-Convert.ToInt32(notify.CustomerNotificationSetting.LastNSecInterval));
            var FilteredListq = PolledTalkList.Where(w => w.SystemID == talk[0] && w.TalkGroupID == talk[1] && w.TransStartUTC > dateTimeNow).ToList();
            foreach (var talkGroupItem in FilteredListq.GroupBy(x => x.TalkGroupID).Select(grp => new { TalkGroupID = grp.Key, TalkGroupItems = grp.ToList() }).ToList().ToList())
            {
                var firstTrans = transmission;
                if (talkGroupItem.TalkGroupItems.Count >= notify.CustomerNotificationSetting.LastNSecTransmissionsCount)
                {
                    string channelName = firstTrans.ChannelName;
                    string radioWithTransmission = string.Empty;
                    string radioWithTransmissionInOrder = string.Join(",", FilteredListq.OrderBy(x => x.TransStartUTC).Select(x => x.TalkGroupItemId).FirstOrDefault());
                    firstTrans.TalkGroupItemId = "";
                    int countTalk = 0;
                    foreach (var trasn in FilteredListq)//talkGroupItem.TalkGroupItems.GroupBy(x => x.RadioID).Select(grp => new { RadioID = grp.Key, TalkGroupItems = grp.ToList() }).ToList())
                    {
                        DateTime dt = Convert.ToDateTime(trasn.TransStartUTC);
                        
                        //foreach (var trasn in transmission1.TalkGroupItems)
                        {
                            radioWithTransmission = radioWithTransmission + String.Format("<br/><br/> <b>Agency Name:</b> {0}  <br/> Transmission play url : <a href='http://rtc.radio1033.com/radio/get_audio.cgi?chunk={1}'>Play</a><br/> Transmission IDs: {1}", trasn.RadioDesc,
                                string.Join(",", trasn.TalkGroupItemId));//Select(x => x.TalkGroupItemId).FirstOrDefault()));
                            firstTrans.TalkGroupItemId = firstTrans.TalkGroupItemId + "" + trasn.TalkGroupItemId + ",";
                        }
                        countTalk++;
                        if (countTalk == notify.CustomerNotificationSetting.LastNSecTransmissionsCount)
                        {
                            await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, Convert.ToDateTime(dt.AddSeconds(Convert.ToInt32(notify.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval))));
                            break;
                        }
                    }
                    firstTrans.TalkGroupItemId = firstTrans.TalkGroupItemId.Remove(firstTrans.TalkGroupItemId.Length - 1, 1);
                    var msg = String.Format("<b>Channel Name:</b>{0}<br/><b>Notification Name : {2} </b> <br/>  {1}  ", channelName, radioWithTransmission, notify.CustomerNotificationSetting.NotificationName);
                    var subject = "Transmission Notification for N transmissions in interval with wait";
                    List<string> userList = new List<string>();
                    List<int> userIdList = new List<int>();
                    var users = CusMap.Join(UserList, cm => cm.UserId, ul => ul.UserId, (cm, ul) => new { cm, ul }).Where(w => w.cm.Active == true && w.cm.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s => new { s.ul.UserId, s.ul.Email }).ToList();
                    foreach (var user in users)
                    {
                        userList.Add(user.Email.ToString());
                        userIdList.Add(user.UserId);
                    }
                    string[] to = userList.Distinct().ToArray();
                    if (((to == null || to.Length == 0)))
                    {
                        return;
                    }
                    SendEmailNotifications(firstTrans, msg, subject, to);
                    LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully for case 3!!"), eventManager);
                }
            }

        }

        private async Task Case1(PolledTalkGroupItem transmission, CustomerModel notify, int notificationId, List<int> talk, DateTime lastScantime, DateTime dateTimeNow)
        {
            //var radioID = CustomerRadios.Where(w => w.CustomerId == notify.CustomerId && w.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s => s.RadioId).ToList();
            //int flag = 0;
            //foreach (var radio in radioID)
            //{
            //    if (radio != 0){
            //        if (transmission.RadioID != radio){
            //            flag++;}
            //    }
            //}

            var intList = CustomerRadios.Where(w => w.CustomerId == notify.CustomerId && w.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s => s.RadioId).ToList();
            List<long> radioID = intList.Select(x => (long)x).ToList();
            int flag = 0;
            foreach (var radio in radioID)
            {
                PolledTalkList = PolledTalkList.Where(w => radioID.Contains(w.RadioID.Value) && w.SystemID == talk[0] && w.TalkGroupID == talk[1] && w.TransStartUTC > dateTimeNow).ToList();
                if (radio != 0)
                {
                    if (transmission.RadioID != radio)
                    {
                        flag++;
                    }
                }
            }

            //await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow);
            if (flag > 0){
                return;}
            dateTimeNow= dateTimeNow.AddSeconds(-Convert.ToInt32(notify.CustomerNotificationSetting.Interval));
            var FilteredListq = PolledTalkList.Where(w => w.SystemID == talk[0] && w.TalkGroupID == talk[1] && w.TransStartUTC > dateTimeNow).ToList();
            foreach (var talkGroupItem in FilteredListq.GroupBy(x => x.TalkGroupID).Select(grp => new { TalkGroupID = grp.Key, TalkGroupItems = grp.ToList() }).ToList().ToList())
            {
                var firstTrans = transmission;
                if (talkGroupItem.TalkGroupItems.Count >= notify.CustomerNotificationSetting.NotifiyTransmissionsCount)
                {
                    string channelName = firstTrans.ChannelName;
                    string radioWithTransmission = string.Empty;
                    string radioWithTransmissionInOrder = string.Join(",", FilteredListq.OrderBy(x => x.TransStartUTC).Select(x => x.TalkGroupItemId).FirstOrDefault());
                    firstTrans.TalkGroupItemId = "";
                    int countTalk = 0;
                    foreach (var trasn in FilteredListq)//talkGroupItem.TalkGroupItems.GroupBy(x => x.RadioID).Select(grp => new { RadioID = grp.Key, TalkGroupItems = grp.ToList() }).ToList())
                    {

                        //await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow);
                        //foreach (var trasn in transmission1.TalkGroupItems)
                        {
                            radioWithTransmission = radioWithTransmission + String.Format("<br/><br/> <b>Agency Name:</b> {0}  <br/> Transmission play url : <a href='http://rtc.radio1033.com/radio/get_audio.cgi?chunk={1}'>Play</a><br/> Transmission IDs: {1}", trasn.RadioDesc,
                                string.Join(",", trasn.TalkGroupItemId));//Select(x => x.TalkGroupItemId).FirstOrDefault()));
                            firstTrans.TalkGroupItemId = firstTrans.TalkGroupItemId + "" + trasn.TalkGroupItemId+",";
                        }
                        countTalk++;
                        if(countTalk==notify.CustomerNotificationSetting.NotifiyTransmissionsCount)
                        {
                            await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, Convert.ToDateTime(trasn.TransStartUTC));
                            break;
                        }
                    }
                    firstTrans.TalkGroupItemId = firstTrans.TalkGroupItemId.Remove(firstTrans.TalkGroupItemId.Length - 1, 1);
                    var msg = String.Format("<b>Channel Name:</b>{0}<br/><b>Notification Name : {2} </b> <br/>  {1}  ", channelName, radioWithTransmission, notify.CustomerNotificationSetting.NotificationName);
                    var subject = "Transmission Notification for N transmissions in interval";
                    List<string> userList = new List<string>();
                    List<int> userIdList = new List<int>();
                    var users = CusMap.Join(UserList, cm => cm.UserId, ul => ul.UserId, (cm, ul) => new { cm, ul }).Where(w => w.cm.Active == true && w.cm.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s => new { s.ul.UserId, s.ul.Email }).ToList();
                    foreach (var user in users)
                    {
                        userList.Add(user.Email.ToString());
                        userIdList.Add(user.UserId);
                    }
                    string[] to = userList.Distinct().ToArray();
                    if (((to == null || to.Length == 0)))
                    {
                        return;
                    }
                    SendEmailNotifications(firstTrans, msg, subject, to);
                    LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully for case 1!!"), eventManager);
                }
            }
        }

        private async Task Case2(PolledTalkGroupItem transmission, CustomerModel notify, int notificationId, List<int> talk, DateTime lastScantime, DateTime dateTimeNow)
        {
            await SqlServerDao.SetLastScannedTimestampUTC(talk, notificationId, dateTimeNow);
            //  User can filter on multiple radio ids. 
            var radioID = CustomerRadios.Where(w => w.CustomerId == notify.CustomerId &&
                w.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s => s.RadioId).FirstOrDefault();
            int flag = 0;
            if (radioID != 0)
            {
                if (transmission.RadioID != radioID)
                {
                    flag++;
                }
            }
            if (flag > 0)
            {
                return;
            }
            var firstTrans = transmission;
            string channelName = firstTrans.ChannelName;
            string radioWithTransmission = String.Format("<br/><br/>Notification Name: {2}<br/> <b>Agency Name:</b> {0} <br/> Transmission play url : <a href='http://rtc.radio1033.com/radio/get_audio.cgi?chunk={1}'>Play</a><br/> Transmission IDs: {1} ", firstTrans.RadioDesc, firstTrans.TalkGroupItemId, notify.CustomerNotificationSetting.NotificationName);
            var msg = String.Format("<b>Channel Name:</b> {0} {1}  ", channelName, radioWithTransmission);
            var subject = "Transmission Notification for First transmission in interval";
            List<string> userList = new List<string>();
            List<int> userIdList = new List<int>();
            var users = CusMap.Join(UserList, cm => cm.UserId, ul => ul.UserId, (cm, ul) => new { cm, ul }).Where(w => w.cm.Active == true && w.cm.CustomerNotificationId == notify.CustomerNotificationSetting.CustomerNotificationId).Select(s => new { s.ul.UserId, s.ul.Email }).ToList();
            foreach (var user in users)
            {
                userList.Add(user.Email.ToString());
                userIdList.Add(user.UserId);
            }
            string[] to = userList.Distinct().ToArray();
            if (((to == null || to.Length == 0)))
            {
                return;
            }
            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Sending email for " + firstTrans.TalkGroupItemId), eventManager);
            SendEmailNotifications(firstTrans, msg, subject, to);
            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully for case 2!!"), eventManager);
            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent for " + firstTrans.TalkGroupItemId), eventManager);
        }

        private static void SendEmailNotifications(PolledTalkGroupItem firstTrans, string msg, string subject, string[] to)
        {
            Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Hello Task library!");
                byte[] fileByteArray = new byte[1];

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Utility.Instance.BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.Timeout = new TimeSpan(0, 59, 0);
                    if (TEST)
                    {
                        String username = "admin";
                        String password = "P5zHfKBpj3";
                        String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                        client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
                    }

                    string URLMP3 = string.Format("radio/get_audio.cgi?chunk={0}", firstTrans.TalkGroupItemId);

                    var response = client.GetAsync(URLMP3).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var responseData = response.Content.ReadAsByteArrayAsync();
                        Console.WriteLine("MP3 downloaded " + firstTrans.TalkGroupItemId);
                        MailService.SendMailAsync(to, null, subject, msg, responseData.Result);
                    }
                }


                //fileByteArray = await GetMP3File(firstTrans.TalkGroupItemId);
                //await MailService.SendMailAsync(to, null, subject, msg, fileByteArray);
                //lastScaned = db.NotificationSettingTalkGroups.ToList();
                //foreach (var usr in userIdList.Distinct().ToList())
                //{
                //    await SqlServerDao.SaveNotificationLog(notify.CustomerId, usr, Convert.ToInt32(firstTrans.TalkGroupID), 0, msg, Encoding.ASCII.GetBytes(firstTrans.TalkGroupItemId));
                //}
            });
        }

        List<CustomerNotificationUserMap> CusMap = new List<CustomerNotificationUserMap>();
        List<User> UserList = new List<User>();

        private async Task NotificationSettingCases()
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                //PolledTalkList = db.PolledTalkGroupItems.Where(w => w.TransStartUTC > date).ToList();
                // Get all notifications from DB
                CustomerRadios = db.NotificationSettingRadioMaps.Select(s => new NotificationSettingRadio() { 
                    CustomerId= s.CustomerId,
                    CustomerNotificationId= s.CustomerNotificationId,
                    NotificationSettingRadioMapId= s.NotificationSettingRadioMapId,
                    RadioId= s.RadioId 
                }).ToList();//.ToList();
                // Get all customers
                var listCustomers = await SqlServerDao.GetCustomerConfiguredNotifications();
                // get all talkgroups for all notifications
                var notification1 = db.NotificationSettingTalkGroups.Select(s => new
                {
                    s.CustomerId,
                    s.CustomerNotificationId,
                    s.NotificationSettingTalkGroupId,
                    s.SelectedCaseID,
                    s.SystemID,
                    s.TakGroupId,
                    s.LastScanUniversalDateTime
                }).ToList();
                // Create notification hash table containing sysid_takgroupid and list of customers who registered for this talkgroup
                NotificationCases.Clear();
                foreach (var notification in notification1)
                {
                    var key = notification.SystemID + "_" + notification.TakGroupId;
                    //CustomerNotification
                    CustomerModel value1 = listCustomers.Where(w => w.CustomerNotificationSetting.CustomerNotificationId == notification.CustomerNotificationId).FirstOrDefault();
                    if (value1 != null)
                    {
                        if (NotificationCases.ContainsKey(key))
                        {
                            NotificationCases[key].Add(value1);
                        }
                        else
                        {
                            NotificationCases.Add(key, new List<CustomerModel> { value1 });
                        }
                    }
                }
                //NotificationCasesTalkGroup.Clear();
                //foreach (var talk in PolledTalkList)
                //{
                //    var key = talk.SystemID + "_" + talk.TalkGroupID;
                //    if (NotificationCasesTalkGroup.ContainsKey(key))
                //    {
                //        NotificationCasesTalkGroup[key].Add(talk);
                //    }
                //    else
                //    {
                //        NotificationCasesTalkGroup.Add(key, new List<PolledTalkGroupItem> { talk });
                //    }
                //}
                CusMap = db.CustomerNotificationUserMaps.Where(w => w.Active == true).ToList();
                UserList = db.Users.Where(w => w.Active == true).ToList();
            }
        }



        //private async Task NotificationSettingCases()
        //{
        //    //var listCustomers = await SqlServerDao.GetAllCustomers();
        //    using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
        //    {
        //        var value = db.CustomerNotificationSettingNews.Select(s => new {
        //            s.Active,s.AfterFirstTransmissionWaitInterval,
        //            s.AfterNSecTransmissionWaitInterval,
        //            s.CustomerId,
        //            s.CustomerNotificationId,
        //            s.Interval,
        //            s.LastNSecTransmissionsCount,
        //            s.NotificationName,
        //            s.NotificationSettingGroupMaps,
        //            s.NotificationSettingRadioMaps,
        //            s.NotificationSettingTalkGroups,
        //            s.NotifiyTransmissionsCount,
        //            s.NSecInterval 
        //        }).ToList();
        //        var notification1 = db.NotificationSettingTalkGroups.Where(w => w.SelectedCaseID == 1).Select(s => new { 
        //        s.CustomerId,
        //        s.CustomerNotificationId,
        //        s.NotificationSettingTalkGroupId,
        //        s.SelectedCaseID,
        //        s.SystemID,
        //        s.TakGroupId                
        //        }).ToList();
        //        foreach (var notification in notification1)
        //        {
        //            var key = notification.SystemID + "_" + notification.TakGroupId;
        //            var value1 = value.Where(w => w.CustomerNotificationId == notification.CustomerNotificationId).FirstOrDefault();
        //            NotificationCases1.Add(key, value1);
        //        }

        //        var notification2 = db.NotificationSettingTalkGroups.Where(w => w.SelectedCaseID == 2).Select(s => new
        //        {
        //            s.CustomerId,
        //            s.CustomerNotificationId,
        //            s.NotificationSettingTalkGroupId,
        //            s.SelectedCaseID,
        //            s.SystemID,
        //            s.TakGroupId
        //        }).ToList();
        //        foreach(var notification in notification2)
        //        {
        //            var key = notification.SystemID + "_" + notification.TakGroupId;
        //            var value1 = value.Where(w => w.CustomerNotificationId == notification.CustomerNotificationId).FirstOrDefault();
        //            NotificationCases2.Add(key, value1);
        //        }

        //        var notification3 = db.NotificationSettingTalkGroups.Where(w => w.SelectedCaseID == 3).Select(s => new
        //        {
        //            s.CustomerId,
        //            s.CustomerNotificationId,
        //            s.NotificationSettingTalkGroupId,
        //            s.SelectedCaseID,
        //            s.SystemID,
        //            s.TakGroupId
        //        }).ToList();
        //        foreach (var notification in notification3)
        //        {
        //            var key = notification.SystemID + "_" + notification.TakGroupId;
        //            var value1 = value.Where(w => w.CustomerNotificationId == notification.CustomerNotificationId).FirstOrDefault();
        //            NotificationCases3.Add(key, value1);
        //        }
        //    }
        //}


        private async Task SendNotificationToCustomers()
        {
            var listCustomers = await SqlServerDao.GetCustomerConfiguredNotifications();
            foreach(var cust in listCustomers.ToList())
            {
                //await semaphoreSlim.WaitAsync();
                try
                {
                    DateTime nextV1Scan = DateTime.Now.ToUniversalTime();
                    nextV1Scan = nextV1Scan.AddSeconds(serverTimeDiffInSeconds);
                    //await ResolveForCustomer(cust, nextV1Scan);
                }
                finally
                {
                   // semaphoreSlim.Release();
                }
            }
        }

        //private async Task ResolveForCustomer(CustomerModel customer, DateTime nextV1Scan)
        //{
        //    try
        //    {
        //        LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Resolve Customer {0}:{1}, Notification Name:{2}", customer.CustomerId, customer.CustomerName, customer.CustomerNotificationSetting.NotificationName), eventManager);

        //        //await semaphoreSlim.WaitAsync();
        //        DateTime lastScanned = await SqlServerDao.GetLastScannedTimestamp(customer.CustomerId, customer.CustomerNotificationSetting.NotificationName);
        //        double totalSeconds = nextV1Scan.Subtract(lastScanned).TotalSeconds;

        //        //Case 1
        //        if (customer.CustomerNotificationSetting != null && customer.CustomerNotificationSetting.Interval != null && customer.CustomerNotificationSetting.NotifiyTransmissionsCount != null
        //            && totalSeconds >= customer.CustomerNotificationSetting.Interval)
        //        {
        //            //int loopCount = Convert.ToInt32(totalSeconds / customer.CustomerNotificationSetting.Interval);
        //            //while (loopCount > 0)
        //            //{
        //            //lastScanned = await SqlServerDao.GetLastScannedTimestamp(customer.CustomerId);
        //            DateTime _nextV1Scan = lastScanned.AddSeconds(Convert.ToInt32(customer.CustomerNotificationSetting.Interval));
        //            await SqlServerDao.SetLastScannedTimestampUTC(customer.CustomerId, _nextV1Scan, customer.CustomerNotificationSetting.NotificationName);
        //            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Start Resolve For Customer {0} from {1}", customer.CustomerId, lastScanned.ToString()), eventManager);

        //            var FilteredList = await SqlServerDao.GetCustomerTransmissions(customer.TalkGroups, lastScanned, _nextV1Scan);

        //            foreach (var talkGroupItem in FilteredList.GroupBy(x => x.TalkGroupID).Select(grp => new { TalkGroupID = grp.Key, TalkGroupItems = grp.ToList() }).ToList().ToList())
        //            {
        //                if (talkGroupItem.TalkGroupItems.Count >= customer.CustomerNotificationSetting.NotifiyTransmissionsCount)
        //                {
        //                    string channelName = talkGroupItem.TalkGroupItems.First().ChannelName;

        //                    string radioWithTransmission = string.Empty;
        //                    string radioWithTransmissionInOrder = string.Join(",", FilteredList.OrderBy(x => x.TransStartUTC).Select(x => x.TalkGroupItemId).ToArray());
        //                    byte[] fileByteArray = new byte[1];
        //                    foreach (var transmission in talkGroupItem.TalkGroupItems.GroupBy(x => x.RadioID).Select(grp => new { RadioID = grp.Key, TalkGroupItems = grp.ToList() }).ToList())
        //                    {
        //                        radioWithTransmission = radioWithTransmission + String.Format("<br/><br/> <b>Agency Name:</b> {0} <br/> Transmission IDs: {1}  ", transmission.TalkGroupItems.First().RadioDesc,
        //                            string.Join(",", transmission.TalkGroupItems.Select(x => x.TalkGroupItemId).ToArray()));
        //                        //radioWithTransmissionInOrder = radioWithTransmissionInOrder + string.Join(",", transmission.TalkGroupItems.OrderBy(x => x.TransStartUTC).Select(x => x.TalkGroupItemId).ToArray());
        //                    }
        //                    fileByteArray = await GetMP3File(radioWithTransmissionInOrder);


        //                    var msg = String.Format("<b>Channel Name:</b> {0} {1}  ", channelName, radioWithTransmission);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, msg, eventManager);
        //                    var subject = "Transmission Notification Case1";

        //                    //var users = (from usr in customer.Users
        //                    //             where usr.TalkGroups.Contains((int)talkGroupItem.TalkGroupID)
        //                    //             select usr).ToList();
        //                    //string[] to = users.Select(x => x.Email).ToArray();
        //                    List<string> userList = new List<string>();
        //                    List<int> userIdList = new List<int>();
        //                    var users = (from grp in customer.Groups
        //                                 where grp.TalkGroups.Contains((int)talkGroupItem.TalkGroupID)
        //                                 select grp.Users).ToList();

        //                    foreach (var user in users)
        //                    {
        //                        userList.AddRange(user.Select(x => x.Email).ToList());
        //                        userIdList.AddRange(user.Select(x => x.UserId).ToList());
        //                    }
        //                    string[] to = userList.Distinct().ToArray();
        //                    if (((to == null || to.Length == 0)))
        //                    {
        //                        continue;
        //                    }
        //                    await MailService.SendMailAsync(to, null, subject, msg, fileByteArray);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully!!"), eventManager);
        //                    foreach (var usr in userIdList.Distinct().ToList())
        //                    {
        //                        await SqlServerDao.SaveNotificationLog(customer.CustomerId, usr, Convert.ToInt32(talkGroupItem.TalkGroupID), 0, msg, fileByteArray);
        //                    }
        //                }
        //            }
        //        }
        //        else if (customer.CustomerNotificationSetting != null && customer.CustomerNotificationSetting.AfterFirstTransmissionWaitInterval != null)//Case 2 (Notify on first transmission and no further notification till 1 hour(config second)
        //        {
        //            await SqlServerDao.SetLastScannedTimestampUTC(customer.CustomerId, nextV1Scan, customer.CustomerNotificationSetting.NotificationName);
        //            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Start Resolve For Customer {0} from {1}", customer.CustomerId, lastScanned.ToString()), eventManager);
        //            foreach (long talkGroup in customer.TalkGroups)
        //            {
        //                var FilteredList = await SqlServerDao.GetCustomerTransmissionsForTalkGroup(talkGroup, lastScanned, nextV1Scan);
        //                var lastSaveSetting = CustomersSettingCases.Where(x => x.CustomerId == customer.CustomerId && x.TalkGroupId == talkGroup && x.CustomerSettingCaseName == "2").FirstOrDefault();
        //                if (FilteredList.Count > 0 && lastSaveSetting == null)
        //                {
        //                    var firstTrans = FilteredList.OrderBy(x => x.TransStartUTC).First();
        //                    nextV1Scan = nextV1Scan.AddSeconds(Convert.ToInt32(customer.CustomerNotificationSetting.AfterFirstTransmissionWaitInterval));
        //                    CustomersSettingCases.Add(new CustomerSettingCase()
        //                    {
        //                        CustomerId = customer.CustomerId,
        //                        TalkGroupId = talkGroup,
        //                        NextScanUniversalDateTime = nextV1Scan,
        //                        CustomerSettingCaseName = "2"
        //                    });

        //                    string channelName = firstTrans.ChannelName;
        //                    string radioWithTransmission = String.Format("<br/><br/> <b>Agency Name:</b> {0} <br/> Transmission IDs: {1}  ", firstTrans.RadioDesc, firstTrans.TalkGroupItemId);
        //                    var msg = String.Format("<b>Channel Name:</b> {0} {1}  ", channelName, radioWithTransmission);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, msg, eventManager);
        //                    var subject = "Transmission Notification Case2";
        //                    List<string> userList = new List<string>();
        //                    List<int> userIdList = new List<int>();
        //                    var users = (from grp in customer.Groups
        //                                 where grp.TalkGroups.Contains((int)talkGroup)
        //                                 select grp.Users).ToList();

        //                    foreach (var user in users)
        //                    {
        //                        userList.AddRange(user.Select(x => x.Email).ToList());
        //                        userIdList.AddRange(user.Select(x => x.UserId).ToList());
        //                    }
        //                    string[] to = userList.Distinct().ToArray();

        //                    if (((to == null || to.Length == 0)))
        //                    {
        //                        continue;
        //                    }

        //                    byte[] fileByteArray = await GetMP3File(firstTrans.TalkGroupItemId);
        //                    await MailService.SendMailAsync(to, null, subject, msg, fileByteArray);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully!!"), eventManager);
        //                    foreach (var usr in userIdList.Distinct().ToList())
        //                    {
        //                        await SqlServerDao.SaveNotificationLog(customer.CustomerId, usr, Convert.ToInt32(firstTrans.TalkGroupID), 0, msg, fileByteArray);
        //                    }
        //                }
        //                else if (FilteredList.Count > 0 && nextV1Scan >= lastSaveSetting.NextScanUniversalDateTime)
        //                {
        //                    var firstTrans = FilteredList.OrderBy(x => x.TransStartUTC).First();
        //                    nextV1Scan = nextV1Scan.AddSeconds(Convert.ToInt32(customer.CustomerNotificationSetting.AfterFirstTransmissionWaitInterval));

        //                    CustomersSettingCases.Where(x => x.CustomerId == customer.CustomerId && x.TalkGroupId == talkGroup && x.CustomerSettingCaseName == "2").ToList().ForEach(x => x.NextScanUniversalDateTime = nextV1Scan);
        //                    string channelName = firstTrans.ChannelName;
        //                    string radioWithTransmission = String.Format("<br/><br/> <b>Agency Name:</b> {0} <br/> Transmission IDs: {1}  ", firstTrans.RadioDesc, firstTrans.TalkGroupItemId);
        //                    var msg = String.Format("<b>Channel Name:</b> {0} {1}  ", channelName, radioWithTransmission);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, msg, eventManager);
        //                    var subject = "Transmission Notification Case2";

        //                    List<string> userList = new List<string>();
        //                    List<int> userIdList = new List<int>();
        //                    var users = (from grp in customer.Groups
        //                                 where grp.TalkGroups.Contains((int)talkGroup)
        //                                 select grp.Users).ToList();

        //                    foreach (var user in users)
        //                    {
        //                        userList.AddRange(user.Select(x => x.Email).ToList());
        //                        userIdList.AddRange(user.Select(x => x.UserId).ToList());
        //                    }
        //                    string[] to = userList.Distinct().ToArray();
        //                    if (((to == null || to.Length == 0)))
        //                    {
        //                        continue;
        //                    }
        //                    byte[] fileByteArray = await GetMP3File(firstTrans.TalkGroupItemId);

        //                    await MailService.SendMailAsync(to, null, subject, msg, fileByteArray);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully!!"), eventManager);
        //                    foreach (var usr in userIdList.Distinct().ToList())
        //                    {
        //                        await SqlServerDao.SaveNotificationLog(customer.CustomerId, usr, Convert.ToInt32(firstTrans.TalkGroupID), 0, msg, fileByteArray);
        //                    }
        //                }
        //            }
        //        }
        //        else if (customer.CustomerNotificationSetting != null && customer.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval != null
        //            && customer.CustomerNotificationSetting.LastNSecInterval != null && customer.CustomerNotificationSetting.LastNSecTransmissionsCount != null)//Case 3 Last X seconds if Y transmisison then notify and not notify till  N sec
        //        {
        //            await SqlServerDao.SetLastScannedTimestampUTC(customer.CustomerId, nextV1Scan, customer.CustomerNotificationSetting.NotificationName);
        //            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Start Resolve For Customer {0} from {1}", customer.CustomerId, lastScanned.ToString()), eventManager);

        //            foreach (long talkGroup in customer.TalkGroups)
        //            {
        //                DateTime fromScan = nextV1Scan.AddSeconds(-Convert.ToInt32(customer.CustomerNotificationSetting.LastNSecInterval - servicePollInterval));
        //                var FilteredList = await SqlServerDao.GetCustomerTransmissionsForTalkGroup(talkGroup, fromScan, nextV1Scan);

        //                var lastSaveSetting = CustomersSettingCases.Where(x => x.CustomerId == customer.CustomerId && x.TalkGroupId == talkGroup && x.CustomerSettingCaseName == "3").FirstOrDefault();
        //                if (lastSaveSetting == null)
        //                {
        //                    lastSaveSetting = new CustomerSettingCase()
        //                    {
        //                        CustomerId = customer.CustomerId,
        //                        TalkGroupId = talkGroup,
        //                        NextScanUniversalDateTime = nextV1Scan,
        //                        CustomerSettingCaseName = "3"
        //                    };
        //                    CustomersSettingCases.Add(lastSaveSetting);
        //                }
        //                if (FilteredList.Count > 0 && FilteredList.Count >= customer.CustomerNotificationSetting.LastNSecTransmissionsCount
        //                    && nextV1Scan >= lastSaveSetting.NextScanUniversalDateTime)
        //                {
        //                    CustomersSettingCases.Where(x => x.CustomerId == customer.CustomerId && x.TalkGroupId == talkGroup && x.CustomerSettingCaseName == "3").ToList().
        //                        ForEach(x => x.NextScanUniversalDateTime = nextV1Scan.AddSeconds(Convert.ToInt32(customer.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval)));

        //                    string channelName = FilteredList.First().ChannelName;
        //                    string radioWithTransmission = string.Empty;
        //                    string radioWithTransmissionInOrder = string.Join(",", FilteredList.OrderBy(x => x.TransStartUTC).Select(x => x.TalkGroupItemId).ToArray());
        //                    byte[] fileByteArray = new byte[1];
        //                    foreach (var transmission in FilteredList.GroupBy(x => x.RadioID).Select(grp => new { RadioID = grp.Key, TalkGroupItems = grp.ToList() }).ToList())
        //                    {
        //                        radioWithTransmission = radioWithTransmission + String.Format("<br/><br/> <b>Agency Name:</b> {0} <br/> Transmission IDs: {1}  ", transmission.TalkGroupItems.First().RadioDesc,
        //                            string.Join(",", transmission.TalkGroupItems.Select(x => x.TalkGroupItemId).ToArray()));
        //                        //radioWithTransmissionInOrder = radioWithTransmissionInOrder + ","+ string.Join(",", transmission.TalkGroupItems.OrderBy(x => x.TransStartUTC).Select(x => x.TalkGroupItemId).ToArray());
        //                    }
        //                    fileByteArray = await GetMP3File(radioWithTransmissionInOrder);
        //                    var msg = String.Format("<b>Channel Name:</b> {0} {1}  ", channelName, radioWithTransmission);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, msg, eventManager);
        //                    var subject = "Transmission Notification Case3";

        //                    //var users = (from usr in customer.Users
        //                    //             where usr.TalkGroups.Contains((int)talkGroup)
        //                    //             select usr).ToList();
        //                    List<string> userList = new List<string>();
        //                    List<int> userIdList = new List<int>();
        //                    var users = (from grp in customer.Groups
        //                                 where grp.TalkGroups.Contains((int)talkGroup)
        //                                 select grp.Users).ToList();

        //                    foreach (var user in users)
        //                    {
        //                        userList.AddRange(user.Select(x => x.Email).ToList());
        //                        userIdList.AddRange(user.Select(x => x.UserId).ToList());
        //                    }
        //                    string[] to = userList.Distinct().ToArray();
        //                    if (((to == null || to.Length == 0)))
        //                    {
        //                        continue;
        //                    }
        //                    await MailService.SendMailAsync(to, null, subject, msg, fileByteArray);
        //                    LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Mail sent successfully!!"), eventManager);
        //                    foreach (var usr in userIdList.Distinct().ToList())
        //                    {
        //                        await SqlServerDao.SaveNotificationLog(customer.CustomerId, usr, Convert.ToInt32(talkGroup), 0, msg, fileByteArray);
        //                    }
        //                }
        //                else if (FilteredList.Count > 0 && FilteredList.Count >= customer.CustomerNotificationSetting.LastNSecTransmissionsCount)//Reset the time
        //                {
        //                    CustomersSettingCases.Where(x => x.CustomerId == customer.CustomerId && x.TalkGroupId == talkGroup && x.CustomerSettingCaseName == "3").ToList().
        //                       ForEach(x => x.NextScanUniversalDateTime = nextV1Scan.AddSeconds(Convert.ToInt32(customer.CustomerNotificationSetting.AfterNSecTransmissionWaitInterval)));
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogMessage.Log(LogMessage.SeverityType.Error, ex.Message, eventManager);
        //    }
        //    finally
        //    {
        //        //semaphoreSlim.Release();
        //    }
        //}

                    
        private DateTime GetLastScannedTimestamp(string projectName, string projectId)
        {
            string value = _profile["LastV1Check" + (projectName != null ? "_" + projectName : "") + (projectId != null ? "_" + projectId.Replace("Scope:", "") : "")].Value;
            return (value != null) ? DateTime.Parse(value) : MIN_DATE;
        }

        private void SetLastScannedTimestampUTC(DateTime timeStamp, string projectName, string projectId)
        {
            _profile["LastV1Check" + (projectName != null ? "_" + projectName : "") + (projectId != null ? "_" + projectId.Replace("Scope:", "") : "")].Value = timeStamp.ToString();
            _profileStore.Flush();
        }

        private async Task<byte[]> GetMP3File(string chunks)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Utility.Instance.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.Timeout = new TimeSpan(0, 59, 0);

                string URLMP3 = string.Format("radio/get_audio.cgi?chunk={0}", chunks);

                HttpResponseMessage response = await client.GetAsync(URLMP3);
                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsByteArrayAsync();
                    //System.IO.File.WriteAllBytes(@"E:\PoliceMonitor6 Backup\WindowService\myFile.mp3", responseData);
                    return responseData;
                }
            }
            return null;
        }

        private async Task Archive_PolledTalkGroupItem(DateTime scanDateTime)
        {
            try
            {
                scanDateTime = scanDateTime.AddHours(-24);
                await SqlServerDao.Archive_PolledTalkGroupItem(scanDateTime);
            }
            catch (Exception ex)
            {
                LogMessage.Log(LogMessage.SeverityType.Error, ex.Message, eventManager);
            }
        }


        //private async Task SendNotificationForTalkGroup(string talkGroupId, string channelName, List<TalkGroupItem> FilteredList)
        //{
        //    try
        //    {
        //        var msg = string.Empty;
        //        long radioId = 0;
        //        LogMessage.Log(LogMessage.SeverityType.Debug, "Start Send Notification For Talk Group", eventManager);

        //        var listUsers = await SqlServerDao.GetUserforTalkGroup(Convert.ToInt64(talkGroupId));
        //        foreach (var talkGroupItem in FilteredList.GroupBy(x => x.RadioID).Select(grp => new { RadioID = grp.Key, TalkGroupItems = grp.ToList() }).ToList().ToList())
        //        {
        //            string IDS = "";
        //            foreach (var transmission in talkGroupItem.TalkGroupItems.ToList())
        //            {
        //                IDS = IDS + " ," + transmission.ID;
        //            }
        //            radioId = talkGroupItem.TalkGroupItems.First().RadioID;
        //            msg = String.Format("Channel Name: {0}, Agency Name: {1}, Transmission IDs: {2}  ", channelName, talkGroupItem.TalkGroupItems.First().RadioDesc, IDS);
        //            LogMessage.Log(LogMessage.SeverityType.Debug, msg, eventManager);
        //            /// LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("ID: {0}, TalkGroupID: {1}, RadioID: {2}, RadioDesc: {3}  ", talkGroupItem.ID, talkGroupItem.TalkGroupID, talkGroupItem.RadioID, talkGroupItem.RadioDesc), eventManager);


        //            //foreach(var usr in listUsers)
        //            //{
        //            //    string IDS="";
        //            //    foreach(var x in FilteredList)
        //            //    {
        //            //        IDS= IDS + " ," +x.RadioID;
        //            //    }
        //            //    LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Customer Name: {0}, UserName: {1}, Talk Group: {2}, ItemID: s{3} ", usr.CustomerName, usr.UserName, talkGroupId, IDS), eventManager);
        //            //}
        //        }
        //        foreach (var usr in listUsers.ToList())
        //        {
        //            //string body = string.Empty;
        //            //body = String.Format("Channel Name: {0}, Agency Name: {1}", channelName, match.CreatedByFirstName, match.CreatedByLastName);
        //            var subject = "Transmission Notification";
        //            string[] to = new string[] { usr.Email };
        //            LogMessage.Log(LogMessage.SeverityType.Debug, String.Format("Sending mail to {0} for talkGroupId {1}.", usr.Email, talkGroupId), eventManager);
        //            //await MailService.SendMailAsync(to, null, subject, msg);
        //            //await SqlServerDao.SaveNotificationLog(usr.CustomerId, usr.UserId, Convert.ToInt32(talkGroupId),(int) radioId, msg);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogMessage.Log(LogMessage.SeverityType.Error, ex.Message, eventManager);

        //    }
        //}
              
    }
}
