﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarrantNotifications.ServiceHost.DAL
{
    public class SQLServerDao
    {
        public string SQLServerConnection { get; set; }

        public SQLServerDao(string connectionString)
        {
            //Initialize SQLServer connection
            this.SQLServerConnection = connectionString;

        }

        public DataTable GetWarrantSubscribeForWarrantType(int warrantType)
        {
            // Provide the query string with a parameter placeholder.
            string queryString =
                "SELECT ProductID, UnitPrice, ProductName from dbo.products "
                    + "WHERE UnitPrice > @pricePoint "
                    + "ORDER BY UnitPrice DESC;";

            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(this.SQLServerConnection))
            using (SqlCommand cmd = new SqlCommand(queryString, con))
            {
                con.Open();
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds);
                    return ds.Tables[0];
                }
            }
        }

  //      SELECT Noti.Name,Noti.TypeID,Noti.Detail,NotiType.Type,Usertab.EmailAddress,Agency.Name
  //FROM [LETG].[dbo].[Notifications]  as Noti inner join [LETG].[dbo].[NotificationType] as NotiType on Noti.TypeID=NotiType.ID
  //inner join [LETG].[dbo].[User] as Usertab on Noti.CreatedBy=Usertab.ID
  //inner join [LETG].[dbo].[Agency] as Agency on Usertab.AgencyID=Agency.ID
  //inner join [LETG].[dbo].[Agency] as Agency on Usertab.AgencyID=Agency.ID
  //where Noti.Active='false
        //Get all items from database into datatable
        public DataTable GetNewWarrants(DateTime datetime)
        {
            // Provide the query string with a parameter placeholder.
            string queryString =
                "SELECT ProductID, UnitPrice, ProductName from dbo.products "
                    + "WHERE UnitPrice > @pricePoint "
                    + "ORDER BY UnitPrice DESC;";

            // string sql = string.Format("select * from users where userName = '{0}' and password = '{1}'", username, password);
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(this.SQLServerConnection))
            using (SqlCommand cmd = new SqlCommand(queryString, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@uname", "");
                cmd.Parameters.AddWithValue("@pwd", "");

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    //User user = new User();
                   // DataRow dr;
                    da.Fill(ds);
                    //dr = ds.Tables[0].Rows[0];

                    //user.Id = Convert.ToInt16(dr["userID"]);
                    //user.FirstName = (string)dr["firstName"];
                    //user.LastName = (string)dr["lastName"];
                    //user.Email = (string)dr["email"];
                    //user.Username = (string)dr["userName"];
                    //user.Password = (string)dr["password"];
                    //user.type = (string)dr["type"];
                    return ds.Tables[0];
                }
            }


        }
    }
}
